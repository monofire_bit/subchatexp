//
//  SubChatUITests.swift
//  SubChatUITests
//
//  Created by ax on 5/23/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import XCTest
@testable import SubChat


class PrivateChatPreviewsListUITests: XCTestCase {

    var app : XCUIApplication?
    let cTableLoadingMaxTimeout = 20.0

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app = XCUIApplication()
        app?.launch()
    }

    override func tearDown() {
        app = nil
        super.tearDown()
    }

    func testIfPrivateChatPreviewsListTableContentExists() {

        self.app?.navigationBars["SubChat.PeopleListView"].buttons["chatIcon"].tap()

        let table = self.app!.tables[cPrivateChatsPreviewsListTableView]

        let cellsAreLoaded = NSPredicate(format: "count > 0")
        expectation(for: cellsAreLoaded, evaluatedWith: table.cells, handler: nil)
        waitForExpectations(timeout: cTableLoadingMaxTimeout, handler: nil)

        let textOfFirstCell = table.cells.element(boundBy: 0).staticTexts.element(boundBy: 0).label

        XCTAssert(table.exists,"FAILED! no table is found")
        XCTAssert(table.cells.count > 0,"FAILED! no cells are loaded")
        XCTAssertFalse(textOfFirstCell.isEmpty, "FAILED! no cell text is loaded")

    }
    
}
