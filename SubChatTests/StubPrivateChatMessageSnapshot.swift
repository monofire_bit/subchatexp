//
//  StubSCPrivateChatMessageSnapshot.swift
//  SubChat
//
//  Created by ax on 6/20/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import Firebase
@testable import SubChat


class StubPrivateChatMessageSnapshot: DataSnapshot{


    var storedValue: [String: AnyObject]
    var storedKey: String

    override init() {


        let messageDataDict: [String : AnyObject] = [
            kMessageCreationTimestamp: NSNumber.init(value: UInt64("1498220531636")!),
            kMessageText: "Fake message text" as AnyObject,
            ]

        storedValue = ["fakeMessageID" : messageDataDict as AnyObject]
        storedKey = "Fake messageID"
    }



    override var value: Any?{
        get{
            return self.storedValue
        }

        set(newValue){
            self.storedValue = newValue as! [String : AnyObject]
        }
    }


    override var key: String{
        get{
            return self.storedKey
        }

        set(newValue){
            self.storedKey = newValue
        }
    }




}




