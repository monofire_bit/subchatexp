//
//  PrivateChatPreviewCellViewModelErrorsTests.swift
//  SubChat
//
//  Created by ax on 7/30/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import UIKit
import XCTest
@testable import SubChat

class PrivateChatPreviewCellViewModelErrorsTests: XCTestCase {
    
    var privateChatPreviewCellViewModel: PrivateChatPreviewCellViewModel?
    
    
    var counterpartyUserImageData : Data?
    
    
    override func setUp() {
        super.setUp()
        
        /* privateChatPreview */
        
        let stubPrivateChatPreview = PrivateChatPreview()
     
        
        /* imageStorage */
        let stubStorage = StubStorage.init(withStubResponceQuality: .RESPOND_ERROR)
        let stubImageStorage = ImageStorage.init(storage: stubStorage)
        
        
        self.privateChatPreviewCellViewModel = PrivateChatPreviewCellViewModel.init(withModel: stubPrivateChatPreview, imageStorage: stubImageStorage)
        
        
        self.privateChatPreviewCellViewModel?.delegate = self
    }
    
    
    
    override func tearDown() {
        self.privateChatPreviewCellViewModel = nil
        super.tearDown()
    }
    
    
    func testIfViewModelIsNotPopulated(){
        
        XCTAssert(self.privateChatPreviewCellViewModel!.counterpartyUserName == "none", "FAILED! has counterpartyUserName in fetch failure case")
        XCTAssert(self.privateChatPreviewCellViewModel!.counterpartyUserAge.isEmpty, "FAILED! has counterpartyUserAge in fetch failure case")
        XCTAssert(self.privateChatPreviewCellViewModel!.lastMessageDate.isEmpty, "FAILED! has lastMessageDate in fetch failure case")

        self.counterpartyUserImageData = self.privateChatPreviewCellViewModel?.counterpartyUserImageData
    }
}





extension PrivateChatPreviewCellViewModelErrorsTests : PrivateChatPreviewCellViewModelDelegate{
    
    func onImageFetched(viewModel: PrivateChatPreviewCellViewModel){
        XCTFail("FAILED! onImageFetched should not be called in fetch failure case")
    }

    func delegateEntity () -> PrivateChatPreviewCellViewModelDelegate{
        return self
    }
    
}

