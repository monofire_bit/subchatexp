//
//  UserDatabaseTests.swift
//  SubChat
//
//  Created by ax on 6/16/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import XCTest
@testable import SubChat

class UserDatabaseTests: XCTestCase, UserDatabaseDelegate {

    var userDatabase: UserDatabase?
    var onUserFetchedExpectation : XCTestExpectation?
    let expectationsTimeout = 0.15

    let testUserIDs = ["testUserID1","testUserID2","testUserID3"]
    let testUserID = "testUserID"
    var indexOfFetchedUser = 0


    override func setUp() {
        super.setUp()
        let stubDatabaseReference = StubDatabaseReference.init(withStubResponceQuality: .RESPOND_WITH_SNAPSHOT)
        self.userDatabase = UserDatabase.init(databaseReference: stubDatabaseReference, userID: self.testUserID)
        indexOfFetchedUser = 0
    }

    override func tearDown() {
        self.userDatabase = nil
        super.tearDown()
    }


    func testIfSuccessfullyFetchesUsersForIDs(){

        self.onUserFetchedExpectation = expectation(description: " onUserFetchedExpectation is called ")
        self.userDatabase!.fetchUsersFor(userIDs: self.testUserIDs, delegate: self)
        waitForExpectations(timeout: expectationsTimeout, handler:nil)
    }



    /* UserDatabaseDelegate protocol */
    func onUserFetched(user: User){
        indexOfFetchedUser += 1

        if (indexOfFetchedUser == testUserIDs.count){
            XCTAssert(user.ID == self.testUserIDs[testUserIDs.count - 1], "UserDatabaseTests ERROR! onUserFetched user.ID is not correct" )
            self.onUserFetchedExpectation?.fulfill()
        }
    }



    func onNoUserExistsForID(userID: String){
        XCTFail("onNoUserExistsForID should not be called in fetch success case")
    }
    
    
    
    
    
}
