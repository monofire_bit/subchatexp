//
//  StubSCPrivateChatUserIDsSnapshot.swift
//  SubChat
//
//  Created by ax on 6/20/17.
//  Copyright © 2017 SubChat. All rights reserved.
//


import Firebase
@testable import SubChat


class StubPrivateChatUserIDsSnapshot: DataSnapshot{


    var storedValue: [String]

    override init() {

        storedValue = [
            "fakeUserID1",
            "fakeUserID2"
        ]
    }

    override var value: Any?{
        get{
            return self.storedValue
        }

        set(newValue){
            self.storedValue = newValue as! [String]
        }
    }




}



