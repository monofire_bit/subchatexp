//
//  StubStorageReference.swift
//  SubChat
//
//  Created by ax on 5/26/17.
//  Copyright © 2017 SubChat. All rights reserved.
//


import Firebase


enum StubStorageReferenceResponceQuality {
    case RESPOND_SUCCESS_WITH_DOWNLOAD_PROGRESS_CALLS
    case RESPOND_SUCCESS_WITHOUT_DOWNLOAD_PROGRESS_CALLS
    case RESPOND_ERROR
}

class StubStorageReference: StorageReference {
    
    let stubResponceQuality: StubStorageReferenceResponceQuality
    
    
    let storageTasksQueue = DispatchQueue.init(label: "stub.sc.storage.tasks.queue", attributes: .concurrent)
    
    let cTestResponceDelayTimeSeconds : Double = 0.01
    
    
    
    init(withStubResponceQuality responceQuality: StubStorageReferenceResponceQuality) {
        stubResponceQuality = responceQuality
    }
    
    
    /* ignore .child calls */
    override func child(_ path: String) -> StorageReference {
        return self
    }
    
    
    override func getData(maxSize size: Int64, completion: @escaping (Data?, Error?) -> Void) -> StorageDownloadTask {
        
        let stubStorageDownloadTask:StorageDownloadTask
     
        switch self.stubResponceQuality {
            
        case .RESPOND_SUCCESS_WITH_DOWNLOAD_PROGRESS_CALLS:
            let fakeData: Data = Data.init(base64Encoded: "fakeData")!
            stubStorageDownloadTask = StubStorageDownloadTask.init(storageTasksQueue: self.storageTasksQueue,
                                                                   onFakeTaskDownloadCompleted: {
                                                                    completion (fakeData, nil)
            })
            
            
            
        case .RESPOND_SUCCESS_WITHOUT_DOWNLOAD_PROGRESS_CALLS:
            let fakeData: Data = Data.init(base64Encoded: "fakeData")!
            stubStorageDownloadTask = StubStorageDownloadTask.init(storageTasksQueue: self.storageTasksQueue,
                                                                   onFakeTaskDownloadCompleted: {
                                                                    completion (fakeData, nil)
            })
            
            let dispatchTime: DispatchTime = DispatchTime.now() + cTestResponceDelayTimeSeconds
            self.storageTasksQueue.asyncAfter(deadline: dispatchTime, execute: {
                completion (fakeData, nil)
            })
            
            
            
            
        case .RESPOND_ERROR:
            stubStorageDownloadTask = StubStorageDownloadTask.init(storageTasksQueue: self.storageTasksQueue,
                                                                   onFakeTaskDownloadCompleted: {
                                                                    completion (nil, nil)
            })
            
        }
        
        
        return stubStorageDownloadTask
    }
    
    
}
