//
//  UserDatabase+PrivateChatsTests.swift
//  SubChat
//
//  Created by ax on 6/20/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import XCTest
@testable import SubChat

class UserDatabase_PrivateChatsTests: XCTestCase, UserDatabasePrivateChatsDelegate {


    var userDatabase: UserDatabase?
    var onPrivateChatIDsFetchedForUserExpectation : XCTestExpectation?
    let expectationsTimeout = 0.1

    let testUserID = "testUserID1"



    override func setUp() {
        super.setUp()

        let stubDatabaseReference = StubDatabaseReference.init(withStubResponceQuality: .RESPOND_WITH_SNAPSHOT)
        userDatabase = UserDatabase.init(databaseReference: stubDatabaseReference, userID: testUserID)
    }



    override func tearDown() {
        userDatabase = nil
        super.tearDown()
    }


    func testIfSuccesfullyFetchedChatIDsForUserID(){
        
        onPrivateChatIDsFetchedForUserExpectation = expectation(description: " onPrivateChatIDsFetchedForUserExpectation is called ")
        userDatabase!.fetchPrivateChatIDsFor(userID: self.testUserID, delegate: self)
        waitForExpectations(timeout: expectationsTimeout, handler:nil)
    }


    /* UserDatabasePrivateChatsDelegate protocol */
    func onPrivateChatIDsFetchedForUser(userID:String, privateChatIDs : Array<String>){

        XCTAssert(self.testUserID == userID, "::: UserDatabase_PrivateChatsTests ERROR! onPrivateChatIDsFetchedForUser userID returned parameter is not correct")
        onPrivateChatIDsFetchedForUserExpectation?.fulfill()
    }


    func onNoPrivateChatsExistForUser(userID:String){
        XCTFail("::: UserDatabase_PrivateChatsTests FAILED! onNoPrivateChatsExistForUser should not be called in fetch success case")
    }

}
