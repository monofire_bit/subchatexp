//
//  PrivateChatsPreviewsListViewModelErrorsTests.swift
//  SubChat
//
//  Created by ax on 7/29/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import XCTest
@testable import SubChat

class PrivateChatsPreviewsListViewModelErrorsTests: XCTestCase {
    
    
    var privateChatsPreviewsListViewModel: PrivateChatsPreviewsListViewModel?
    
    
    var onNoPrivateChatsExistExpectation : XCTestExpectation?
    let expectationsTimeout = 0.15
    
    
    let testUserID = "testUserID"
    
    
    override func setUp() {
        super.setUp()
        
        /* privateChatsPreviewsList */
        let stubUserDatabaseReference = StubDatabaseReference.init(withStubResponceQuality: .RESPOND_EMPTY_SNAPSHOT)
        let userDatabase = UserDatabase.init(databaseReference: stubUserDatabaseReference, userID: self.testUserID)
        
        let stubPrivateChatDatabaseReference = StubDatabaseReference.init(withStubResponceQuality: .RESPOND_EMPTY_SNAPSHOT)
        let privateChatDatabase = PrivateChatDatabase.init(databaseReference: stubPrivateChatDatabaseReference)
        
        
        let privateChatsPreviewsList = PrivateChatsPreviewsList.init(withUserID: testUserID,
                                                                     userDatabase: userDatabase,
                                                                     privateChatDatabase: privateChatDatabase)
        
        
        /* imageStorage */
        let stubStorage = StubStorage.init(withStubResponceQuality: .RESPOND_ERROR)
        let stubImageStorage = ImageStorage.init(storage: stubStorage)
        
        
        
        self.privateChatsPreviewsListViewModel = PrivateChatsPreviewsListViewModel.init(withModel: privateChatsPreviewsList,
                                                                                        imageStorage: stubImageStorage, maxImagesNumberToBePrefetched: 1)
        
        self.privateChatsPreviewsListViewModel?.delegate = self
    }
    
    
    
    override func tearDown() {
        self.privateChatsPreviewsListViewModel = nil
        super.tearDown()
    }
    
    
    func testIfSuccessfullyFetchesPrivateChatsPreviewsList(){
        
        self.onNoPrivateChatsExistExpectation = expectation(description: " onNoPrivateChatsExist is called ")
        self.privateChatsPreviewsListViewModel?.fetchPrivateChatsPreviews()
        waitForExpectations(timeout: expectationsTimeout, handler:nil)
    }
}




extension PrivateChatsPreviewsListViewModelErrorsTests : PrivateChatsPreviewsListViewModelDelegate{
    func onPrivateChatsPreviewsListUpdated(){
        XCTFail("::: PrivateChatsPreviewsListErrorsTests FAILED! onPrivateChatsPreviewsListFetched should not be called in no chats exists case")
    }
    
    
    func onNoPrivateChatsExist(){
        self.onNoPrivateChatsExistExpectation?.fulfill()
    }
}
