//
//  StubSCPrivateChatIDsSnapshot.swift
//  SubChat
//
//  Created by ax on 6/20/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import Firebase
@testable import SubChat


class StubPrivateChatIDsSnapshot: DataSnapshot{


    var storedValue: [String]

    override init() {

        storedValue = [
            "fakePrivateChatID1",
            "fakePrivateChatID2",
            "fakePrivateChatID3"
        ]
    }

    override var value: Any?{
        get{
            return self.storedValue
        }

        set(newValue){
            self.storedValue = newValue as! [String]
        }
    }

    
}


