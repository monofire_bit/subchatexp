//
//  PrivateChatsPreviewsListTests.swift
//  SubChat
//
//  Created by ax on 7/6/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import XCTest
@testable import SubChat

class PrivateChatsPreviewsListTests: XCTestCase, PrivateChatsPreviewsListDelegate {



    var privateChatsPreviewsList: PrivateChatsPreviewsList?

    var onPrivateChatsPreviewsListFetchedExpectation : XCTestExpectation?
    let expectationsTimeout = 0.15


    let testUserID = "testUserID"
    let testPrivateChatID = "testPrivateChatID1"


    override func setUp() {
        super.setUp()

        let stubUserDatabaseReference = StubDatabaseReference.init(withStubResponceQuality: .RESPOND_WITH_SNAPSHOT)
        let userDatabase = UserDatabase.init(databaseReference: stubUserDatabaseReference, userID: self.testUserID)

        let stubPrivateChatDatabaseReference = StubDatabaseReference.init(withStubResponceQuality: .RESPOND_WITH_SNAPSHOT)
        let privateChatDatabase = PrivateChatDatabase.init(databaseReference: stubPrivateChatDatabaseReference)


        self.privateChatsPreviewsList = PrivateChatsPreviewsList.init(withUserID: testUserID,
                                                                      userDatabase: userDatabase,
                                                                      privateChatDatabase: privateChatDatabase)
        self.privateChatsPreviewsList!.delegate = self
    }


    override func tearDown() {
        self.privateChatsPreviewsList = nil
        super.tearDown()
    }


    func testIfSuccessfullyFetchesPrivateChatsPreviewsListForUserID(){

        self.onPrivateChatsPreviewsListFetchedExpectation = expectation(description: " onPrivateChatsPreviewsListFetchedExpectation is called ")
        self.privateChatsPreviewsList!.fetchPrivateChatsPreviewsList()
        waitForExpectations(timeout: expectationsTimeout, handler:nil)
    }


    
    /* PrivateChatsPreviewsListDelegate protocol */

    func onPrivateChatsPreviewsListFetched(privateChatsPreviewsList: Array<PrivateChatPreview>, forUserID userID: String) {

        XCTAssert(userID == self.testUserID, "::: PrivateChatsPreviewsListTests ERROR! onPrivateChatsPreviewsListFetched userID is not correct" )
        self.onPrivateChatsPreviewsListFetchedExpectation?.fulfill()
    }
    
    
    func onNoPrivateChatsExist(forUserID userID: String){
        XCTFail("::: PrivateChatsPreviewsListTests ERROR! onNoPrivateChatsExist should not be called in fetch success case")
    }
    
    
}
