//
//  ImageStorageTests.swift
//  SubChat
//
//  Created by ax on 5/23/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import XCTest
@testable import SubChat


class ImageStorageTests: XCTestCase,ImageStorageDelegate {

    var imageStorage: ImageStorage?
    var onImageFetchedExpectation : XCTestExpectation?
    var onImageFetchProgress : XCTestExpectation?

    let expectationsTimeout = 0.1


    override func setUp() {
        super.setUp()

        let stubStorage = StubStorage.init(withStubResponceQuality: .RESPOND_SUCCESS_WITH_DOWNLOAD_PROGRESS_CALLS)
        imageStorage = ImageStorage.init(storage: stubStorage)
    }


    override func tearDown() {
        imageStorage = nil
        super.tearDown()

    }

    func testIfSuccessfullyFetchesImagesForLinks(){

        onImageFetchedExpectation = expectation(description: " onImageFetched is called ")
        onImageFetchProgress = expectation(description: " onImageFetchProgress is called ")

        imageStorage!.fetchImagesFor(imagesLinks: ["testImageLink"], delegate: self)
        waitForExpectations(timeout: expectationsTimeout, handler:nil)
    }




    /* ImageStorageDelegate protocol */
    func onImageFetchError(error: Error?, imageLink: String){

        DispatchQueue.main.async {
            XCTFail("onImageFetchError should not be called in fetch success case")
        }
    }

    func onImageFetched(imageData: Data, imageLink: String){
        DispatchQueue.main.async {
            self.onImageFetchedExpectation?.fulfill()
        }
    }


    func onImageFetchProgress(progress: Float, imageLink: String){

        DispatchQueue.main.async {
            if (progress > 0.95){
                self.onImageFetchProgress?.fulfill()
            }
        }
    }
    
    
    
}
