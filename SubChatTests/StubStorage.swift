//
//  StubStorage.swift
//  SubChat
//
//  Created by ax on 7/26/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import Firebase
class StubStorage: Storage{

    let stubResponceQuality: StubStorageReferenceResponceQuality

    init(withStubResponceQuality responceQuality: StubStorageReferenceResponceQuality) {
        stubResponceQuality = responceQuality
    }

    override func reference(forURL string: String) -> StorageReference{
        return StubStorageReference.init(withStubResponceQuality: self.stubResponceQuality)
    }



}
