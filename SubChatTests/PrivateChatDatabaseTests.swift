//
//  PrivateChatDatabaseTests.swift
//  SubChat
//
//  Created by ax on 6/20/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import XCTest
import Firebase
@testable import SubChat


class PrivateChatDatabaseTests: XCTestCase,
    PrivateChatDatabaseDelegate,
    PrivateChatDatabaseMessagesDelegate,
PrivateChatDatabaseUsersDelegate{



    var privateChatDatabase: PrivateChatDatabase?
    var onPrivateChatFetchedForIDExpectation : XCTestExpectation?
    var onLastMessageFetchedForPrivateChatExpectation : XCTestExpectation?
    var onUserIDsFetchedForPrivateChatExpectation : XCTestExpectation?

    let expectationsTimeout = 0.1

    let testUserIDs = ["testUserID1","testUserID2","testUserID3"]
    let testPrivateChatID = "testPrivateChatID1"





    override func tearDown() {
        privateChatDatabase = nil
        super.tearDown()
    }



    func testIfSuccessfullyFetchesPrivateChatForID(){

        let stubDatabaseReference = StubDatabaseReference.init(withStubResponceQuality: .RESPOND_WITH_SNAPSHOT)
        privateChatDatabase = PrivateChatDatabase.init(databaseReference: stubDatabaseReference)
        onPrivateChatFetchedForIDExpectation = expectation(description: " onPrivateChatFetchedForIDExpectation is called ")
        privateChatDatabase?.fetchPrivateChatFor(privateChatID: self.testPrivateChatID, delegate: self)
        waitForExpectations(timeout: expectationsTimeout, handler:nil)
    }


    /* PrivateChatDatabaseUsersDelegate protocol */
    func onPrivateChatFetched(privateChat: PrivateChat){


        XCTAssert(privateChat.ID == testPrivateChatID,
                  "SCCPrivateChatDatabase_MessagesTests ERROR! onPrivateChatFetchedForID privateChat.ID is not correct" )


        onPrivateChatFetchedForIDExpectation?.fulfill()

    }



    func onNoPrivateChatExistsForID (privateChatID : String){
        XCTFail("onNoPrivateChatExistsForID should not be called in fetch success case")
    }





    func testIfSuccessfullyFetchesLastMessageForPrivateChatID(){
        let stubDatabaseReference = StubDatabaseReference.init(withStubResponceQuality: .RESPOND_WITH_SNAPSHOT)
        privateChatDatabase = PrivateChatDatabase.init(databaseReference: stubDatabaseReference)

        onLastMessageFetchedForPrivateChatExpectation = expectation(description: " onLastMessageFetchedForPrivateChatExpectation is called ")
        privateChatDatabase!.fetchLastMessageFor(privateChatID: self.testPrivateChatID, delegate: self)
        waitForExpectations(timeout: expectationsTimeout, handler:nil)
    }




    /* PrivateChatDatabaseMessagesDelegate protocol */
    func onLastMessageFetchedForPrivateChat(privateChatID : String, lastMessage: Message){

        XCTAssert(privateChatID == self.testPrivateChatID,
                  "SCCPrivateChatDatabase_MessagesTests ERROR! onLastMessageFetchedForPrivateChat userID returned parameter is not correct")
        onLastMessageFetchedForPrivateChatExpectation?.fulfill()
    }


    func onNoMessagesExistForPrivateChat(privateChatID : String){
        XCTFail("onNoMessagesExistForPrivateChat should not be called in fetch success case")
    }





    func testIfSuccessfullyFetchesUserIDsForPrivateChatID(){

        let stubDatabaseReference = StubDatabaseReference.init(withStubResponceQuality: .RESPOND_WITH_SNAPSHOT)
        privateChatDatabase = PrivateChatDatabase.init(databaseReference: stubDatabaseReference)


        onUserIDsFetchedForPrivateChatExpectation = expectation(description: " onUserIDsFetchedForPrivateChatExpectation is called ")
        privateChatDatabase!.fetchUsersFor(privateChatID: self.testPrivateChatID, delegate: self)
        waitForExpectations(timeout: expectationsTimeout, handler:nil)
    }




    /* PrivateChatDatabaseUsersDelegate protocol */
    func onUserIDsFetchedForPrivateChat(privateChatID: String, userIDs: Array <String>){
        XCTAssert(privateChatID == self.testPrivateChatID,
                  "onUserIDsFetchedForPrivateChat ERROR! onLastMessageFetchedForPrivateChat userID returned parameter is not correct")
        onUserIDsFetchedForPrivateChatExpectation?.fulfill()
    }
    
    
    func onNoUsersExistForPrivateChat(privateChatID: String){
        XCTFail("onNoUsersExistForPrivateChat should not be called in fetch success case")
    }
    
}

