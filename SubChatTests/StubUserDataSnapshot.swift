//
//  StubUserSnapshot.swift
//  SubChat
//
//  Created by ax on 6/16/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import Firebase
@testable import SubChat


class StubUserSnapshot: DataSnapshot {


    var storedValue: [String: AnyObject]

    override init() {

        storedValue = [
            kUserCreationTimestamp: NSNumber.init(value: UInt64("1499689600184")!),
            kUserName: "Dirty Joe" as AnyObject,
            kDayOfBirth: "1" as AnyObject,
            kMonthOfBirth: "2" as AnyObject,
            kYearOfBirth: "1991" as AnyObject,
            kUserEducation: "Joe's dirty education" as AnyObject,
            kUserJob: "Joe's dirty job" as AnyObject,
            kUserAbout: "Joe is dirty" as AnyObject,
            kUserFbLink: "fb.com/dirtyJoe" as AnyObject,
            kUserAvatarImageLink: "firebase.com/joeAvatar.jpg" as AnyObject,
            kUserSmallAvatarImageLink: "firebase.com/joeSmallAvatar.jpg" as AnyObject,
        ]

    }

    override var value: Any?{
        get{
            return self.storedValue
        }
        
        set(newValue){
            self.storedValue = newValue as! [String : AnyObject]
        }
    }



}


