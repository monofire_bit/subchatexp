//
//  PrivateChatDatabaseErrorsTests.swift
//  SubChat
//
//  Created by ax on 6/20/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import XCTest
import Firebase
@testable import SubChat


class PrivateChatDatabaseErrorsTests: XCTestCase,
    PrivateChatDatabaseDelegate,
    PrivateChatDatabaseMessagesDelegate,
PrivateChatDatabaseUsersDelegate{



    var privateChatDatabase: PrivateChatDatabase?
    var onPrivateChatFetchedForIDExpectation : XCTestExpectation?
    var onNoMessagesExistForPrivateChatExpectation : XCTestExpectation?
    var onNoUsersExistForPrivateChatExpectation : XCTestExpectation?

    let expectationsTimeout = 0.1

    let testUserIDs = ["testUserID1","testUserID2","testUserID3"]
    let testPrivateChatID = "testPrivateChatID1"



    override func setUp() {
        super.setUp()
        let stubDatabaseReference = StubDatabaseReference.init(withStubResponceQuality: .RESPOND_EMPTY_SNAPSHOT)
        privateChatDatabase = PrivateChatDatabase.init(databaseReference: stubDatabaseReference)


    }


    override func tearDown() {
        privateChatDatabase = nil
        super.tearDown()
    }



    func testIfRaisesErrorOnInvalidPrivateChatForIDFetchResult(){

        let stubDatabaseReference = StubDatabaseReference.init(withStubResponceQuality: .RESPOND_EMPTY_SNAPSHOT)
        privateChatDatabase = PrivateChatDatabase.init(databaseReference: stubDatabaseReference)
        onPrivateChatFetchedForIDExpectation = expectation(description: " onPrivateChatFetchedForIDExpectation is called ")
        privateChatDatabase?.fetchPrivateChatFor(privateChatID: self.testPrivateChatID, delegate: self)
        waitForExpectations(timeout: expectationsTimeout, handler:nil)
    }


    /* PrivateChatDatabaseUsersDelegate protocol */
    func onPrivateChatFetched(privateChat: PrivateChat){
        XCTFail("onPrivateChatFetchedForID should not be called in fetch failed case")
    }


    func onNoPrivateChatExistsForID (privateChatID : String){
        XCTAssert(privateChatID == self.testPrivateChatID,
                  "SCCPrivateChatDatabase_MessagesTests ERROR! onNoPrivateChatExistsForID userID returned parameter is not correct")
        onPrivateChatFetchedForIDExpectation?.fulfill()
    }








    func testIfRaisesErrorOnInvalidLastMessageFetchResult(){

        onNoMessagesExistForPrivateChatExpectation = expectation(description: " onNoMessagesExistForPrivateChatExpectation is called ")
        privateChatDatabase!.fetchLastMessageFor(privateChatID: self.testPrivateChatID, delegate: self)
        waitForExpectations(timeout: expectationsTimeout, handler:nil)
    }



    /* PrivateChatDatabaseMessagesDelegate protocol */
    func onLastMessageFetchedForPrivateChat(privateChatID : String, lastMessage: Message){
        XCTFail("onLastMessageFetchedForPrivateChat should not be called in fetch failed case")
    }


    func onNoMessagesExistForPrivateChat(privateChatID : String){

        XCTAssert(privateChatID == self.testPrivateChatID,
                  "SCCPrivateChatDatabase_MessagesTests ERROR! onLastMessageFetchedForPrivateChat userID returned parameter is not correct")
        onNoMessagesExistForPrivateChatExpectation?.fulfill()
    }








    
    func testIfRaisesErrorOnInvalidUserIDsFetchResult(){

        onNoUsersExistForPrivateChatExpectation = expectation(description: " onNoUsersExistForPrivateChatExpectation is called ")
        privateChatDatabase!.fetchUsersFor(privateChatID: self.testPrivateChatID, delegate: self)
        waitForExpectations(timeout: expectationsTimeout, handler:nil)
    }




    /* PrivateChatDatabaseUsersDelegate protocol */
    func onUserIDsFetchedForPrivateChat(privateChatID: String, userIDs: Array <String>){
        XCTFail("onUserIDsFetchedForPrivateChat should not be called in fetch failed case")
    }


    func onNoUsersExistForPrivateChat(privateChatID: String){

        XCTAssert(privateChatID == testPrivateChatID,
                  "onUserIDsFetchedForPrivateChat ERROR! onLastMessageFetchedForPrivateChat userID returned parameter is not correct")
        
        onNoUsersExistForPrivateChatExpectation?.fulfill()
    }
    
}


