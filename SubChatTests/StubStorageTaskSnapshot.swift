//
//  StubStorageTaskSnapshot.swift
//  SubChat
//
//  Created by ax on 6/6/17.
//  Copyright © 2017 SubChat. All rights reserved.
//


import Firebase

class StubStorageTaskSnapshot: StorageTaskSnapshot {

    var storedProgress: Progress?

    override init() {
        storedProgress = Progress()
    }


    override var progress: Progress?{
        get{
            return self.storedProgress
        }

        set(newProgress){
            self.storedProgress = newProgress
        }
    }

    
}


