//
//  ImageStorageErrorsTests.swift
//  SubChat
//
//  Created by ax on 6/13/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import XCTest
@testable import SubChat


class ImageStorageErrorsTests: XCTestCase, ImageStorageDelegate {

    var imageStorage: ImageStorage?
    var onImageFetchedWithErrorExpectation : XCTestExpectation?
    var onImageFetchProgress : XCTestExpectation?

    let expectationsTimeout = 0.2


    override func setUp() {
        super.setUp()
        let stubStorage = StubStorage.init(withStubResponceQuality: .RESPOND_ERROR)
        imageStorage = ImageStorage.init(storage: stubStorage)
    }
    
    override func tearDown() {
        imageStorage = nil
        super.tearDown()
    }



    func testIfRaisesErrorOnInvalidFetchResult(){
        onImageFetchedWithErrorExpectation = expectation(description: "onImageFetchError is called")
        onImageFetchProgress = expectation(description: " onImageFetchProgress is called ")

        imageStorage?.fetchImagesFor(imagesLinks: ["testImageLink"], delegate: self)
        waitForExpectations(timeout: expectationsTimeout, handler:nil)
    }




    /* ImageStorageDelegate protocol */
    func onImageFetchError(error: Error?, imageLink: String){
        onImageFetchedWithErrorExpectation?.fulfill()
    }

    func onImageFetched(imageData: Data, imageLink: String){
        XCTFail("onImageFetched should not be called in fetch failed case")
    }


    func onImageFetchProgress(progress: Float, imageLink: String){
        if (progress > 0.95){
            onImageFetchProgress?.fulfill()
        }
    }




}
