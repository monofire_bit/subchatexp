//
//  StubStorageDownloadTask.swift
//  SubChat
//
//  Created by ax on 5/26/17.
//  Copyright © 2017 SubChat. All rights reserved.
//


import Firebase



class StubStorageDownloadTask: StorageDownloadTask {

    let cTestResponceDelayTimeSeconds: Double = 0.01
    let cTestProgressStepsNumber = 5
    let onFakeTaskDownloadCompletedHandler : (() -> Void)?





    var storageTasksQueue : DispatchQueue


    init(storageTasksQueue : DispatchQueue , onFakeTaskDownloadCompleted :  (() -> Void)?) {
        self.onFakeTaskDownloadCompletedHandler = onFakeTaskDownloadCompleted
        self.storageTasksQueue = storageTasksQueue
    }



    override func observe(_ status: StorageTaskStatus, handler: @escaping (StorageTaskSnapshot) -> Void) -> String {

        for progressStep in 0...cTestProgressStepsNumber {
            dispatchNewProgress(progressStep: progressStep, handler: handler)
        }

        return "StorageHandler stub"
    }




    func dispatchNewProgress(progressStep: Int, handler: @escaping (StorageTaskSnapshot) -> Void ) {

        let delayTime: Double = Double(progressStep) * cTestResponceDelayTimeSeconds
        let dispatchTime: DispatchTime = DispatchTime.now() + delayTime


        storageTasksQueue.asyncAfter(deadline: dispatchTime) {

            let storageTaskSnapshot = StubStorageTaskSnapshot()

            storageTaskSnapshot.progress?.completedUnitCount = Int64(progressStep)
            storageTaskSnapshot.progress?.totalUnitCount = Int64(self.cTestProgressStepsNumber)
            handler(storageTaskSnapshot)
            self.reportIfTaskIsCompleted(completedUnitCount: Int64(progressStep), totalUnitCount: Int64(self.cTestProgressStepsNumber))
        }
    }



    func reportIfTaskIsCompleted(completedUnitCount: Int64, totalUnitCount: Int64){
        if (completedUnitCount == totalUnitCount && onFakeTaskDownloadCompletedHandler != nil){
            self.onFakeTaskDownloadCompletedHandler!()
        }
    }
    
}

