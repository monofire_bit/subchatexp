//
//  StubUITableViewCell.swift
//  SubChat
//
//  Created by ax on 8/22/17.
//  Copyright © 2017 SubChat. All rights reserved.
//


import UIKit
import XCTest
@testable import SubChat

class StubUITableViewCell : UITableViewCell{

    var onImageFetchedExpectation : XCTestExpectation!
    var testIndexPath : IndexPath!
  


    convenience init (withExpectation expectation: XCTestExpectation, testIndexPath: IndexPath){
        self.init(style: .default, reuseIdentifier: nil)
        self.testIndexPath = testIndexPath
        self.onImageFetchedExpectation = expectation
    }

}


extension StubUITableViewCell : PrivateChatPreviewCellViewModelDelegate{

    func onImageFetched(viewModel: PrivateChatPreviewCellViewModel){

        XCTAssert(viewModel.counterpartyUserImageData!.count > 0, "FAILED! no counterpartyUserImageData")
        self.onImageFetchedExpectation.fulfill()
    }

    
    func delegateEntity () -> PrivateChatPreviewCellViewModelDelegate{
        self.indexPathTag = self.testIndexPath
        return self
    }
    
    
}
