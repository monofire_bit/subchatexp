//
//  StubSCPrivateChatSnapshot.swift
//  SubChat
//
//  Created by ax on 6/23/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import Firebase
@testable import SubChat

class StubPrivateChatSnapshot: DataSnapshot {


    var storedValue: [String: AnyObject]

    override init() {

        storedValue = [
            kPrivateChatCreationTimestamp: NSNumber.init(value: UInt64("1499689600184")!)
        ]
    }


    override var value: Any?{
        get{
            return self.storedValue
        }

        set(newValue){
            self.storedValue = newValue as! [String : AnyObject]
        }
    }


}




