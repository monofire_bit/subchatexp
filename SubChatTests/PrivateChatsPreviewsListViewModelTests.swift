//
//  PrivateChatsPreviewsListViewModelTests.swift
//  SubChat
//
//  Created by ax on 7/29/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import XCTest
@testable import SubChat

class PrivateChatsPreviewsListViewModelTests: XCTestCase {
    
    
    var privateChatsPreviewsListViewModel: PrivateChatsPreviewsListViewModel?
    
    
    var onPrivateChatsPreviewsListFetchedExpectation : XCTestExpectation?
    let expectationsTimeout = 0.15
    
    
    let testUserID = "testUserID"
    
    
    override func setUp() {
        super.setUp()
        
        /* privateChatsPreviewsList */
        let stubUserDatabaseReference = StubDatabaseReference.init(withStubResponceQuality: .RESPOND_WITH_SNAPSHOT)
        let userDatabase = UserDatabase.init(databaseReference: stubUserDatabaseReference, userID: self.testUserID)
        
        let stubPrivateChatDatabaseReference = StubDatabaseReference.init(withStubResponceQuality: .RESPOND_WITH_SNAPSHOT)
        let privateChatDatabase = PrivateChatDatabase.init(databaseReference: stubPrivateChatDatabaseReference)
        
        
        let privateChatsPreviewsList = PrivateChatsPreviewsList.init(withUserID: testUserID,
                                                                     userDatabase: userDatabase,
                                                                     privateChatDatabase: privateChatDatabase)
        
        
        /* imageStorage */
        let stubStorage = StubStorage.init(withStubResponceQuality: .RESPOND_SUCCESS_WITHOUT_DOWNLOAD_PROGRESS_CALLS)
        let stubImageStorage = ImageStorage.init(storage: stubStorage)
        
        
        
        self.privateChatsPreviewsListViewModel = PrivateChatsPreviewsListViewModel.init(withModel: privateChatsPreviewsList,
                                                                                        imageStorage: stubImageStorage, maxImagesNumberToBePrefetched: 1)
        
        self.privateChatsPreviewsListViewModel?.delegate = self
    }
    
    
    
    override func tearDown() {
        self.privateChatsPreviewsListViewModel = nil
        super.tearDown()
    }
    
    
    func testIfSuccessfullyFetchesPrivateChatsPreviewsList(){
        
        self.onPrivateChatsPreviewsListFetchedExpectation = expectation(description: " onPrivateChatsPreviewsListFetched is called ")
        self.privateChatsPreviewsListViewModel?.fetchPrivateChatsPreviews()
        waitForExpectations(timeout: expectationsTimeout, handler:nil)
    }
}




extension PrivateChatsPreviewsListViewModelTests : PrivateChatsPreviewsListViewModelDelegate{
    func onPrivateChatsPreviewsListUpdated(){
        
        XCTAssert((self.privateChatsPreviewsListViewModel?.cellViewModels.count)! > 0,
                  "::: PrivateChatsPreviewsListViewModelTests FAILED no View Models found")
        
        self.onPrivateChatsPreviewsListFetchedExpectation?.fulfill()
    }
    
    
    func onNoPrivateChatsExist(){
        XCTFail("::: PrivateChatsPreviewsListViewModelTests FAILED! onNoPrivateChatsExist should not be called in fetch success case")
    }
}
