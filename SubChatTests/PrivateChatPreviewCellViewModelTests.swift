//
//  PrivateChatPreviewCellViewModelTests.swift
//  SubChat
//
//  Created by ax on 7/29/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import UIKit
import XCTest
@testable import SubChat

class PrivateChatPreviewCellViewModelTests: XCTestCase {
    
    var privateChatPreviewCellViewModel: PrivateChatPreviewCellViewModel?
    var stubCell : StubUITableViewCell?
    let cTestIndexPath = IndexPath.init(row: 10, section: 0)
    

    let expectationsTimeout = 0.15
    
    
    var counterpartyUserImageData : Data?


    
    override func setUp() {
        super.setUp()
        
        /* privateChatPreview */
        
        var stubPrivateChatPreview = PrivateChatPreview()
        stubPrivateChatPreview.privateChatCreationTimestamp = "1499689600184"
        stubPrivateChatPreview.lastMessage = Message.init (fakeRandomMessage: "foo")
        stubPrivateChatPreview.counterpartyUser = User.init (fakeRandomUserWithImageLink: "fakeImageLink1", smallImageLink: "smallFakeImageLink1")
        
        
        /* imageStorage */
        let stubStorage = StubStorage.init(withStubResponceQuality: .RESPOND_SUCCESS_WITHOUT_DOWNLOAD_PROGRESS_CALLS)
        let stubImageStorage = ImageStorage.init(storage: stubStorage)
        
        self.privateChatPreviewCellViewModel = PrivateChatPreviewCellViewModel.init(withModel: stubPrivateChatPreview, imageStorage: stubImageStorage)
        self.privateChatPreviewCellViewModel?.linkedCellIndexPathTag = cTestIndexPath
    }
    
    
    
    override func tearDown() {
        self.privateChatPreviewCellViewModel = nil
        self.stubCell = nil
        super.tearDown()
    }
    
    
    func testIfViewModelIsPopulated(){
        
        XCTAssert(!self.privateChatPreviewCellViewModel!.counterpartyUserName.isEmpty, "FAILED! no counterpartyUserName")
        XCTAssert(!self.privateChatPreviewCellViewModel!.distanceToUser.isEmpty, "FAILED! no distanceToUser")
        XCTAssert(!self.privateChatPreviewCellViewModel!.counterpartyUserAge.isEmpty, "FAILED! no counterpartyUserAge")
        XCTAssert(!self.privateChatPreviewCellViewModel!.lastMessageDate.isEmpty, "FAILED! no lastMessageDate")


        let onImageFetchedExpectation = expectation(description: " onImageFetched is called ")
        self.stubCell = StubUITableViewCell.init(withExpectation: onImageFetchedExpectation, testIndexPath :cTestIndexPath)
        self.privateChatPreviewCellViewModel?.delegate = self.stubCell

        self.counterpartyUserImageData = self.privateChatPreviewCellViewModel?.counterpartyUserImageData
        waitForExpectations(timeout: self.expectationsTimeout, handler:nil)

    }
}





