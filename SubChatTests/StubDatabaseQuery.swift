//
//  StubDatabaseQuery.swift
//  SubChat
//
//  Created by ax on 6/20/17.
//  Copyright © 2017 SubChat. All rights reserved.
//


import Firebase
class StubDatabaseQuery: DatabaseQuery {


    let cTestResponceDelayTimeSeconds = 0.02

    let databaseTasksQueue : DispatchQueue 
    let stubSnapshot : DataSnapshot

    init(withStubSnapshot stubSnapshot: DataSnapshot, dispatchQueue : DispatchQueue) {
        self.databaseTasksQueue = dispatchQueue
        self.stubSnapshot = stubSnapshot
    }


    override func observeSingleEvent(of eventType: DataEventType, with block: @escaping (DataSnapshot) -> Void) {

        let dispatchTime: DispatchTime = DispatchTime.now() + cTestResponceDelayTimeSeconds

        self.databaseTasksQueue.asyncAfter(deadline: dispatchTime) {
            block(self.stubSnapshot)
        }
    }
    
}
