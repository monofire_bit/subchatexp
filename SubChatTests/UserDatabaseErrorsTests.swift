//
//  UserDatabaseErrorsTests.swift
//  SubChat
//
//  Created by ax on 6/16/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import XCTest
@testable import SubChat


class UserDatabaseErrorsTests : XCTestCase, UserDatabaseDelegate {

    var userDatabase: UserDatabase?
    var onNoUserExistsForIDExpectation : XCTestExpectation?
    let expectationsTimeout = 0.15

    let testUserIDs = ["testUserID1","testUserID2","testUserID3"]
    let testUserID = "testUserID"
    var indexOfFetchedUser = 0


    override func setUp() {
        super.setUp()
        let stubDatabaseReference = StubDatabaseReference.init(withStubResponceQuality: .RESPOND_EMPTY_SNAPSHOT)
        self.userDatabase = UserDatabase.init(databaseReference: stubDatabaseReference, userID: self.testUserID)
        indexOfFetchedUser = 0
    }

    override func tearDown() {
        self.userDatabase = nil
        super.tearDown()
    }


    func testIfRaisedErrorOnInvalidFetchResult(){

        self.onNoUserExistsForIDExpectation = expectation(description: " onNoUserExistsForIDExpectation is called ")
        self.userDatabase!.fetchUsersFor(userIDs: self.testUserIDs, delegate: self)
        waitForExpectations(timeout: expectationsTimeout, handler:nil)
    }


    /* UserDatabaseDelegate protocol */
    func onUserFetched(user: User){
         XCTFail("::: UserDatabaseErrorsTests FAILED! onUserFetched should not be called in fetch failed case")
    }


    func onNoUserExistsForID(userID: String){

        indexOfFetchedUser += 1

        if (indexOfFetchedUser == testUserIDs.count){
            onNoUserExistsForIDExpectation?.fulfill()
        }
    }
}
