//
//  StubDatabaseReference.swift
//  SubChat
//
//  Created by ax on 6/16/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import Firebase
@testable import SubChat


enum StubDatabaseReferenceResponceQuality {
    case RESPOND_WITH_SNAPSHOT
    case RESPOND_EMPTY_SNAPSHOT
}


class StubDatabaseReference: DatabaseReference {


    let cTestResponceDelayTimeSeconds = 0.02

    let databaseTasksQueue = DispatchQueue.init(label: "stub.database.query.tasks.queue",
                                                        qos: .utility)

    var stubSnapshot : DataSnapshot
    let responceQuality: StubDatabaseReferenceResponceQuality
    var snapshotPath : String?



    init(withStubResponceQuality responceQuality: StubDatabaseReferenceResponceQuality) {
        self.responceQuality = responceQuality
        self.stubSnapshot = DataSnapshot() /* value is empty*/

    }



    override func child(_ path: String) -> DatabaseReference {
        self.snapshotPath = path
        return self
    }



    override func observeSingleEvent(of eventType: DataEventType,
                                     with block: @escaping (DataSnapshot) -> Void) {

        let dispatchTime: DispatchTime = DispatchTime.now() + cTestResponceDelayTimeSeconds

        
        let capturedSnapthotPath = self.snapshotPath!
        let capturedResponceQuality = self.responceQuality
        
        self.databaseTasksQueue.asyncAfter(deadline: dispatchTime) { [unowned self] in

            let snapShot = self.snapshotFromPath(path: capturedSnapthotPath, responceQuality: capturedResponceQuality)
            block(snapShot)
        }
    }



    override func queryLimited(toLast limit: UInt) -> DatabaseQuery {


        let  snapShot = self.snapshotFromPath(path: self.snapshotPath!, responceQuality: self.responceQuality)
        return StubDatabaseQuery(withStubSnapshot:snapShot, dispatchQueue: self.databaseTasksQueue)
    }



    func snapshotFromPath(path : String,  responceQuality: StubDatabaseReferenceResponceQuality) -> DataSnapshot {

        if ( self.responceQuality == .RESPOND_EMPTY_SNAPSHOT){
            return DataSnapshot() /* value is empty*/
        }

        switch path {

        case cUserPrivateChatIDsNode:
            return StubPrivateChatIDsSnapshot()

        case cPrivateChatDataNode:
            return StubPrivateChatSnapshot()

        case cPrivateChatMessagesNode:
            return StubPrivateChatMessageSnapshot()

        case cUserDataNode:
            return StubUserSnapshot()

        case cPrivateChatUsersNode:
            return StubPrivateChatUserIDsSnapshot()
            
        default:
            break
        }
        
        return DataSnapshot() /* value is empty*/
    }
    
    
    
    
}

