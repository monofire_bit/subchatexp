//
//  UserDatabase+PrivateChatsErrorsTests.swift
//  SubChat
//
//  Created by ax on 6/20/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import XCTest
@testable import SubChat

class UserDatabase_PrivateChatsErrorsTests: XCTestCase, UserDatabasePrivateChatsDelegate {


    var userDatabase: UserDatabase?
    var onNoPrivateChatsExistForUserExpectation : XCTestExpectation?
    let expectationsTimeout = 0.1
    let testUserID = "testUserID1"



    override func setUp() {
        super.setUp()

        let stubDatabaseReference = StubDatabaseReference.init(withStubResponceQuality: .RESPOND_EMPTY_SNAPSHOT)
        userDatabase = UserDatabase.init(databaseReference: stubDatabaseReference, userID: testUserID)
    }



    override func tearDown() {
        userDatabase = nil
        super.tearDown()
    }



    func testIfSuccesfullyFetchedChatIDsForUserID(){

        onNoPrivateChatsExistForUserExpectation = expectation(description: " onNoPrivateChatsExistForUserExpectation is called ")
        userDatabase!.fetchPrivateChatIDsFor(userID: self.testUserID, delegate: self)
        waitForExpectations(timeout: expectationsTimeout, handler:nil)
    }


    /* UserDatabasePrivateChatsDelegate protocol */
    func onPrivateChatIDsFetchedForUser(userID:String, privateChatIDs : Array<String>){
        XCTFail("::: UserDatabase_PrivateChatsErrorsTests FAILED!  onPrivateChatIDsFetchedForUser should not be called in fetch failed case")
    }


    func onNoPrivateChatsExistForUser(userID:String){
        XCTAssert(self.testUserID == userID, "::: UserDatabase_PrivateChatsErrorsTests FAILED! onPrivateChatIDsFetchedForUser userID returned parameter is not correct")
        onNoPrivateChatsExistForUserExpectation?.fulfill()
    }
    
}


