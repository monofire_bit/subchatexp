//
//  PrivateChatsPreviewsListErrorTests.swift
//  SubChat
//
//  Created by ax on 7/11/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import XCTest
@testable import SubChat

class PrivateChatsPreviewsListErrorsTests: XCTestCase, PrivateChatsPreviewsListDelegate {



    var privateChatsPreviewsList: PrivateChatsPreviewsList?

    var onNoPrivateChatsExistExpectation : XCTestExpectation?
    let expectationsTimeout = 0.15


    let testUserID = "testUserID"
    let testPrivateChatID = "testPrivateChatID1"


    override func setUp() {
        super.setUp()

        let stubUserDatabaseReference = StubDatabaseReference.init(withStubResponceQuality: .RESPOND_EMPTY_SNAPSHOT)
        let userDatabase = UserDatabase.init(databaseReference: stubUserDatabaseReference, userID: self.testUserID)

        let stubPrivateChatDatabaseReference = StubDatabaseReference.init(withStubResponceQuality: .RESPOND_EMPTY_SNAPSHOT)
        let privateChatDatabase = PrivateChatDatabase.init(databaseReference: stubPrivateChatDatabaseReference)


        self.privateChatsPreviewsList = PrivateChatsPreviewsList.init(withUserID: testUserID,
                                                                      userDatabase: userDatabase,
                                                                      privateChatDatabase: privateChatDatabase)
        self.privateChatsPreviewsList!.delegate = self
    }


    override func tearDown() {
        self.privateChatsPreviewsList = nil
        super.tearDown()
    }


    func testIfRaisesErrorOnInvalidPrivateChatsPreviewsListFetchResult(){

        self.onNoPrivateChatsExistExpectation = expectation(description: " onNoPrivateChatsExistExpectation is called ")
        self.privateChatsPreviewsList!.fetchPrivateChatsPreviewsList()
        waitForExpectations(timeout: expectationsTimeout, handler:nil)
    }



    /* PrivateChatsPreviewsListDelegate protocol */

    func onPrivateChatsPreviewsListFetched(privateChatsPreviewsList: Array<PrivateChatPreview>, forUserID userID: String) {
        XCTAssert(userID == self.testUserID, "::: PrivateChatsPreviewsListErrorsTests ERROR! onPrivateChatsPreviewsListFetched userID is not correct" )
        XCTFail("::: PrivateChatsPreviewsListErrorsTests FAILED! onNoPrivateChatsExist should not be called in fetch success case")
    }


    func onNoPrivateChatsExist(forUserID userID: String){

        XCTAssert(userID == self.testUserID, "::: PrivateChatsPreviewsListErrorsTests ERROR! onNoPrivateChatsExist userID is not correct" )
        self.onNoPrivateChatsExistExpectation?.fulfill()

    }
    
    
}
