//
//  FirebaseService.swift
//  SubChat
//
//  Created by ax on 7/24/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import Firebase


class FirebaseService {

    lazy var imageStorage = ImageStorage.init(storage:  Storage.storage())

    let databaseReference : DatabaseReference


    //MARK: --- Lifecycle

    init() {
        let databaseTasksQueue = DispatchQueue.init(label: "firebase.database.tasks.queue")
        Database.database().callbackQueue = databaseTasksQueue
        self.databaseReference = Database.database().reference()
    }



}
