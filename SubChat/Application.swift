//
//  Application.swift
//  SubChat
//
//  Created by ax on 7/24/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import UIKit
import Firebase

class Application{

    //MARK: --- Application Dependencies

    private let window: UIWindow

    lazy var firebaseService = FirebaseService()

    lazy var navigationCoordinator: NavigationCoordinator = NavigationCoordinator(
        window: self.window,
        navigationController: NavigationViewController(),
        application: self
    )

    
    //MARK: --- Lifecycle

    init(window: UIWindow) {
        self.window = window
        self.initServices()
    }


    private func initServices(){
        FirebaseApp.configure()
    }

}

