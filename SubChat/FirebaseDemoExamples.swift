//
//  FirebaseDemoExamples.swift
//  SubChat
//
//  Created by ax on 6/22/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import Firebase



/* Populates develop database with fake data */

class FirebaseDemoExamples {


    let cUserAvatarLinksPath = "gs://fir-bee55.appspot.com/"
    let cUserAvatarSmallLinksPath = "gs://fir-bee55.appspot.com/"
    let cImageNamePrefix = "user"

    let cNumberOfFakePrivateChats = 60
    let cNumberOfMessages = 4

    func resetDB(){
        let baseReference = Database.database().reference()
        baseReference.setValue(nil)
    }



    func populateDevDB(){

        var privateChatIDs = Array<String>()
        var userIDs = Array<String>()

        /* private chats */
        self.addFakePrivateChats(numberOfFakePrivateChats: self.cNumberOfFakePrivateChats) { (privateChatIDsHandler) in
            privateChatIDs = privateChatIDsHandler


            /* users */

            let numberOfFakeUsers = self.cNumberOfFakePrivateChats + 1
            /* users are always + 1 more than chats - this +1 is allocated for app user, hile others are counterparty users */

            self.addFakeUsers(numberOfFakeUsers:numberOfFakeUsers, privateChatsIDs: privateChatIDs) { (userIDsHandler) in
                userIDs = userIDsHandler

                /* userIDs for private chats */
                self.addUserIDsForPrivateChats(userIDs: userIDs, privateChatIDs: privateChatIDs)

                /* messages for private chats */
                self.addFakeMessagesForPrivateChats(privateChatIDs: privateChatIDs, numberOfMessages: self.cNumberOfMessages)

            }
        }
    }





    /* Fake Users */

    func addFakeUsers(numberOfFakeUsers:Int, privateChatsIDs: Array<String>, userIDs: @escaping (Array<String>) -> Void){

        let baseReference = Database.database().reference()
        var userIDsArray = Array <String>()


        for i in 0..<numberOfFakeUsers {

            let fakeRandomUserWithImageLink = cUserAvatarLinksPath + cImageNamePrefix + String(i+1) + ".jpg"
            let smallImageLink = cUserAvatarSmallLinksPath + cImageNamePrefix + String(i+1) + ".jpg"


            let fakeUser = User.init(fakeRandomUserWithImageLink: fakeRandomUserWithImageLink, smallImageLink: smallImageLink)
            let firebaseValue = fakeUser.firebaseValue()

            let userID = baseReference.child(cUsersNode).childByAutoId().key
            userIDsArray.append(userID)

            baseReference.child(cUsersNode).child(userID).child(cUserDataNode).setValue(firebaseValue)
            baseReference.child(cUsersNode).child(userID).child(cUserPrivateChatIDsNode).setValue(privateChatsIDs)
        }

        userIDs(userIDsArray)
    }




    /* Fake Private Chats  */

    func addFakePrivateChats(numberOfFakePrivateChats:Int, privateChatIDs: @escaping (Array<String>) -> Void){

        let baseReference = Database.database().reference()
        var privateChatIDsArray = Array <String>()

        for _ in 0..<numberOfFakePrivateChats {

            let fakePrivateChat = PrivateChat.init(fakeRandomPrivateChat: "")
            let firebaseValue = fakePrivateChat.firebaseValue()
            let privateChatID = baseReference.child(cPrivateChatsNode).childByAutoId().key

            privateChatIDsArray.append(privateChatID)
            baseReference.child(cPrivateChatsNode).child(privateChatID).child(cPrivateChatDataNode).setValue(firebaseValue)
        }

        privateChatIDs(privateChatIDsArray)
    }



    /* Private Chat User IDs */
    func addUserIDsForPrivateChats(userIDs : Array <String>, privateChatIDs : Array <String>){

        for (index,privateChatID) in privateChatIDs.enumerated(){

            let referenceToPrivateChat = Database.database().reference().child(cPrivateChatsNode).child(privateChatID)

            /* [userIDs[0] - app user      userIDs[index+1] - counterparty user */
            referenceToPrivateChat.updateChildValues([cPrivateChatUsersNode:[userIDs[0],userIDs[index+1]]])
        }
    }



    /* Fake messages */
    func addFakeMessagesForPrivateChats (privateChatIDs : Array <String>, numberOfMessages: Int){


        for privateChatID in privateChatIDs{
            let referenceToPrivateChat = Database.database().reference().child(cPrivateChatsNode).child(privateChatID).child(cPrivateChatMessagesNode)


            for _ in 0..<numberOfMessages{
                let fakeMessage = Message.init(fakeRandomMessage: "")
                let firebaseMessageValue = fakeMessage.firebaseValue()
                
                let messageID = referenceToPrivateChat.childByAutoId().key
                
                /* add message */
                referenceToPrivateChat.updateChildValues([messageID : firebaseMessageValue])
            }
        }
        
    }
    
    

}
