//
//  PrivateChatsPreviewsListViewModel.swift
//  SubChat
//
//  Created by ax on 7/12/17.
//  Copyright © 2017 SubChat. All rights reserved.
//
import Foundation
import UIKit

protocol PrivateChatsPreviewsListViewModelDelegate: class {
    func onNoPrivateChatsExist()
    func onPrivateChatsPreviewsListUpdated()
}


protocol PrivateChatsPreviewsListViewModelUIEventsDelegate: class {
    func onPrivateChatPreviewSelected(chatPreview: PrivateChatPreview)
}


class PrivateChatsPreviewsListViewModel {

    /* Delegates */
    weak var delegate : PrivateChatsPreviewsListViewModelDelegate?
    weak var uiEventsDelegate : PrivateChatsPreviewsListViewModelUIEventsDelegate?


    /* Cells */
    fileprivate(set) var cellViewModels = Array <CellDeploying>()
    let tableCellViewModelTypes: [CellDeploying.Type] = [PrivateChatPreviewCellViewModel.self]



    /* Filtering */
    fileprivate(set) var cellViewModelsNonFiltered = Array <CellDeploying>()
    fileprivate let filterTasksSerialQueue = DispatchQueue.init(label: "private.chats.previewa.view.model.tasks.queue", qos: .utility)

    /* Model */
    private let privateChatsPreviewsList: PrivateChatsPreviewsList
    fileprivate let imageStorage : ImageStorage


    /* Images prefetch */
    lazy var prefetchGroup = DispatchGroup()
    fileprivate var maxImagesNumberToBePrefetched: Int = 0
    fileprivate let cImagesPrefetchMaxTimeout = 5.0





    //MARK: --- Lifecycle

    init(withModel model: PrivateChatsPreviewsList, imageStorage : ImageStorage, maxImagesNumberToBePrefetched: Int) {
        self.privateChatsPreviewsList = model
        self.imageStorage = imageStorage
        self.privateChatsPreviewsList.delegate = self
        self.maxImagesNumberToBePrefetched = maxImagesNumberToBePrefetched
    }


    //MARK: --- Fetch

    func fetchPrivateChatsPreviews(){
        self.privateChatsPreviewsList.fetchPrivateChatsPreviewsList()
    }

}










//MARK: --- Filter

extension PrivateChatsPreviewsListViewModel{

    func filterPrivateChatsPreviewsBy(counterpartyUserName: String?){

        guard let filterPhrase = counterpartyUserName, !filterPhrase.isEmpty else {
            self.resetFilter()
            return
        }

        self.filterTasksSerialQueue.async {


            if ( self.cellViewModelsNonFiltered.isEmpty) {
                self.cellViewModelsNonFiltered = self.cellViewModels
            }

            self.cellViewModels = self.cellViewModelsNonFiltered.filter({ cellDeploying in

                let cellViewModel = cellDeploying as! PrivateChatPreviewCellViewModel
                return cellViewModel.counterpartyUserName.lowercased().contains(filterPhrase.lowercased())

            })

            guard let delegate = self.delegate else {
                DispatchQueue.main.async {}
                return
            }

            DispatchQueue.main.async {
                delegate.onPrivateChatsPreviewsListUpdated()
            }
        }
    }


    func resetFilter(){

        guard !self.cellViewModelsNonFiltered.isEmpty else{
            return
        }

        self.cellViewModels = self.cellViewModelsNonFiltered
        self.cellViewModelsNonFiltered.removeAll()
        self.delegate?.onPrivateChatsPreviewsListUpdated()
    }

}






//MARK: --- Model

extension PrivateChatsPreviewsListViewModel: PrivateChatsPreviewsListDelegate {


    func onPrivateChatsPreviewsListFetched(privateChatsPreviewsList: Array <PrivateChatPreview>, forUserID userID: String){
        self.populateViewModels(privateChatsPreviewsList: privateChatsPreviewsList)
        self.prefetchImagesIfAny()
    }


    func onNoPrivateChatsExist(forUserID userID: String){
        guard let delegate = self.delegate else {
            DispatchQueue.main.async {}
            return
        }

        DispatchQueue.main.async {
            delegate.onNoPrivateChatsExist()
        }
    }


    private func populateViewModels(privateChatsPreviewsList: Array <PrivateChatPreview>){

        self.cellViewModels = privateChatsPreviewsList.map({ [unowned self] (privateChatPreview) -> PrivateChatPreviewCellViewModel  in

            let cellViewModel = PrivateChatPreviewCellViewModel.init(withModel: privateChatPreview, imageStorage: self.imageStorage)

            /* subscribe */
            cellViewModel.uiEventsDelegate = self

            return cellViewModel
        })
    }
}



//MARK: --- Cell calls

extension PrivateChatsPreviewsListViewModel : PrivateChatPreviewCellViewModelUIEventsDelegate{

    func onCellSelected(with privateChatPreview: PrivateChatPreview) {
        self.uiEventsDelegate?.onPrivateChatPreviewSelected(chatPreview: privateChatPreview)
    }
}





//MARK: --- Fetch images with dispatch group

extension PrivateChatsPreviewsListViewModel{


    fileprivate func prefetchImagesIfAny(){

        guard self.maxImagesNumberToBePrefetched > 0 else {

            guard let delegate = self.delegate else {
                DispatchQueue.main.async {}
                return
            }

            DispatchQueue.main.async {
                delegate.onPrivateChatsPreviewsListUpdated()
            }
            return
        }


        let imagesCount = (self.maxImagesNumberToBePrefetched <= self.cellViewModels.count)
            ? self.maxImagesNumberToBePrefetched : self.cellViewModels.count

        self.prefetchGroup = DispatchGroup()

        for  i in 0..<imagesCount {
            let viewModel = self.cellViewModels[i] as! PrivateChatPreviewCellViewModel

            prefetchGroup.enter()
            viewModel.fetchCounterPartyImageInGroup(dispatchGroup: prefetchGroup)
        }


        let timeout: DispatchTime = DispatchTime.now() + cImagesPrefetchMaxTimeout
        guard self.prefetchGroup.wait(timeout: timeout) == .success else{

            guard let delegate = self.delegate else {
                DispatchQueue.main.async {}
                return
            }
            DispatchQueue.main.async {
                delegate.onPrivateChatsPreviewsListUpdated()
            }
            return
        }
        
        
        self.prefetchGroup.notify(queue: DispatchQueue.main) {
            guard let delegate = self.delegate else {
                DispatchQueue.main.async {}
                return
            }
            delegate.onPrivateChatsPreviewsListUpdated()
        }

    }
}




