//
//  UITableViewCell+Tagging.swift
//  SubChat
//
//  Created by ax on 8/18/17.
//  Copyright © 2017 SubChat. All rights reserved.
//


import ObjectiveC
import UIKit


private struct CellAssociatedObject {
    static var indexPathTag = "indexPathTag"
}


extension UITableViewCell: CellTagging {

    var indexPathTag: IndexPath? {

        get { return objc_getAssociatedObject(self, &CellAssociatedObject.indexPathTag) as? IndexPath }

        set {

            /* reset associated object value */
            objc_setAssociatedObject(self, &CellAssociatedObject.indexPathTag, nil, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)

            if let newValue = newValue {
                objc_setAssociatedObject(self, &CellAssociatedObject.indexPathTag, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
}
