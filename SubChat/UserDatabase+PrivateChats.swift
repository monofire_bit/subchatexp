//
//  UserDatabase+PrivateChats.swift
//  SubChat
//
//  Created by ax on 6/15/17.
//  Copyright © 2017 SubChat. All rights reserved.
//


protocol UserDatabasePrivateChatsDelegate {
    func onPrivateChatIDsFetchedForUser(userID:String, privateChatIDs : Array<String>)
    func onNoPrivateChatsExistForUser(userID:String)

}


extension UserDatabase{

    func fetchPrivateChatIDsFor(userID: String, delegate: UserDatabasePrivateChatsDelegate?){

        /* compose reference*/
        let userPrivateChatIDsReference = self.databaseReference.child(cUsersNode).child(userID).child(cUserPrivateChatIDsNode)

        /* request data */
        userPrivateChatIDsReference.observeSingleEvent(of: .value, with: { (snapshot) in

            guard snapshot.exists(), snapshot.value != nil else{
                delegate?.onNoPrivateChatsExistForUser(userID: userID)
                return
            }
            
            let privateChatIDs = snapshot.value as! Array<String>
            delegate?.onPrivateChatIDsFetchedForUser(userID: userID, privateChatIDs: privateChatIDs)
            
        })
        
    }
    
}

