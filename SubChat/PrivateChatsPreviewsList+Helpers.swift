//
//  PrivateChatsPreviewsList+Helpers.swift
//  SubChat
//
//  Created by ax on 6/29/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import Foundation
extension PrivateChatsPreviewsList{


    //MARK: --- Fetch Timeout Timer
    func armTimer(){

        self.timerIsArmed = true

        let dispatchTime: DispatchTime = DispatchTime.now() + cFetchTimeoutSeconds
        self.concurrentAccessQueuePrivateChatsPreviewsList.asyncAfter(deadline: dispatchTime){ [weak self] in

            guard let weakSelf = self else {
                return
            }

            if (weakSelf.timerIsArmed){
                weakSelf.timerIsArmed = false
                let delegate : PrivateChatsPreviewsListDelegate? = weakSelf.delegate
                weakSelf.delegate = nil
                let sortedPrivateChatPreviewsList = weakSelf.sortedPrivateChatPreviewsList(unsortedPrivateChatsPreviewsList:  weakSelf.privateChatsPreviewsList)
                delegate?.onPrivateChatsPreviewsListFetched(privateChatsPreviewsList: sortedPrivateChatPreviewsList, forUserID: weakSelf.myUserID)
            }
        }
    }


    func disarmTimer(){

        if (self.timerIsArmed){
            self.timerIsArmed = false
        }
    }




    //MARK: --- Counterparty User ID
    func myCounterpartyUserIDFromPrivateChatUserIDs(userIDs: Array <String>) -> String{

        for (i,userID) in userIDs.enumerated(){

            if (userID != self.myUserID){
                return userID
            }
            assert(i == 0, "::: PrivateChatsPreviewsList ERROR! wrong counterparty userID, both private chat users has myUserID")
        }
        return ""
    }



    //MARK: --- Events
    func notifyDelegateWithFetchCompletion(){
        self.disarmTimer()
        let sortedPrivateChatPreviewsList = self.sortedPrivateChatPreviewsList(unsortedPrivateChatsPreviewsList:  self.privateChatsPreviewsList)
        self.delegate?.onPrivateChatsPreviewsListFetched(privateChatsPreviewsList: sortedPrivateChatPreviewsList, forUserID: self.myUserID)
    }
    
}
