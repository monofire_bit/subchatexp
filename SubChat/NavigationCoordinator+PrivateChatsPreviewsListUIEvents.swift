//
//  NavigationCoordinator+PrivateChatsPreviewsListUIEvents.swift
//  SubChat
//
//  Created by ax on 8/10/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import UIKit


extension NavigationCoordinator : PrivateChatsPreviewsListViewModelUIEventsDelegate{

    func onPrivateChatPreviewSelected(chatPreview: PrivateChatPreview) {
        print ( "Cell selected. Cell selected. UserID -> \(chatPreview.counterpartyUser?.ID ?? "n/a ")   name -> \(chatPreview.counterpartyUser?.name ?? "n/a ")")

        if self.navigationController.presentedViewController != nil {
            self.navigationController.dismiss(animated: true, completion: {
                self.showPrivateChat(for: chatPreview.counterpartyUser?.name ?? "n/a")
            })
        } else {
            self.showPrivateChat(for: chatPreview.counterpartyUser?.name ?? "n/a")
        }
    }
    
}
