//
//  PrivateChatsPreviewsList+BuildingStatus.swift
//  SubChat
//
//  Created by ax on 7/5/17.
//  Copyright © 2017 SubChat. All rights reserved.
//


struct PrivateChatListBuildingStatus {

    var privateChatFetchActiveRequests : Int
    var messageFetchActiveRequests : Int
    var userIdFetchActiveRequests : Int
    var userFetchActiveRequests : Int


    init() {
        self.privateChatFetchActiveRequests = 0
        self.messageFetchActiveRequests = 0
        self.userIdFetchActiveRequests = 0
        self.userFetchActiveRequests = 0
    }
}



/* Track end of private chat list building process.
 Building is completed when all async fetch requests are completed */

extension PrivateChatsPreviewsList{

    func resetBuildingStatus(){
        self.buildingStatus.privateChatFetchActiveRequests = 0
        self.buildingStatus.messageFetchActiveRequests = 0
        self.buildingStatus.userIdFetchActiveRequests = 0
        self.buildingStatus.userFetchActiveRequests = 0
    }


    func modifyPrivateChatFetchActiveRequestsCount( delta: Int ){

        self.concurrentAccessQueueUserPrivateChat.async (flags: .barrier){
            [unowned self] in
            self.buildingStatus.privateChatFetchActiveRequests += delta
        }
    }


    func modifyMessageFetchActiveRequestsCount( delta: Int ){

        self.concurrentAccessQueueUserPrivateChat.async (flags: .barrier){
            [unowned self] in
            self.buildingStatus.messageFetchActiveRequests += delta
        }
    }


    func modifyUserIdFetchActiveRequestsCount( delta: Int ){

        self.concurrentAccessQueueUserPrivateChat.async (flags: .barrier){
            [unowned self] in
            self.buildingStatus.userIdFetchActiveRequests += delta
        }
    }

    func modifyUserFetchActiveRequestsCount( delta: Int ){

        self.concurrentAccessQueueUserPrivateChat.async (flags: .barrier){
            [unowned self] in
            self.buildingStatus.userFetchActiveRequests += delta
        }
    }



    func buildProcessIsCompleted() -> Bool {

        assert(self.buildingStatus.privateChatFetchActiveRequests >= 0,"::: ERROR! PrivateChatsPreviewsList privateChatFetchActiveRequests is invalid")
        assert(self.buildingStatus.messageFetchActiveRequests >= 0,"::: ERROR! PrivateChatsPreviewsList messageFetchActiveRequests is invalid")
        assert(self.buildingStatus.userIdFetchActiveRequests >= 0,"::: ERROR! PrivateChatsPreviewsList userIdFetchActiveRequests is invalid")
        assert(self.buildingStatus.userFetchActiveRequests >= 0,"::: ERROR! PrivateChatsPreviewsList userFetchActiveRequests is invalid")


        var completionStatus = false
        self.concurrentAccessQueueUserPrivateChat.sync (flags: .barrier){


            let allChatsAreProcessed =  self.buildingStatus.privateChatFetchActiveRequests == 0
            let allMessagesAreProcessed = self.buildingStatus.messageFetchActiveRequests == 0
            let allUserIDsAreProcessed = self.buildingStatus.userIdFetchActiveRequests == 0
            let allUsersAreProcessed = self.buildingStatus.userFetchActiveRequests == 0

            if (allChatsAreProcessed && allMessagesAreProcessed && allUserIDsAreProcessed && allUsersAreProcessed){
                completionStatus = true
            }
        }
        return completionStatus
    }
}
