//
//  UIImage+RoundedImage.swift
//  SubChat
//
//  Created by ax on 8/11/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import UIKit
extension UIImage{

    func roundedImage() -> UIImage {
        let imageLayer = CALayer()

        imageLayer.frame = CGRect.init(x: 0.0, y: 0.0, width: self.size.width, height: self.size.height)
        imageLayer.contents = self.cgImage

        imageLayer.masksToBounds = true
        imageLayer.cornerRadius = imageLayer.bounds.width / 2.0

        UIGraphicsBeginImageContext(self.size)
        imageLayer.render(in: UIGraphicsGetCurrentContext()!)
        let roundedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return roundedImage!
    }


}
