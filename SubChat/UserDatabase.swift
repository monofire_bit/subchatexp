//
//  UserDatabase.swift
//  SubChat
//
//  Created by ax on 6/15/17.
//  Copyright © 2017 SubChat. All rights reserved.
//



import Firebase

protocol UserDatabaseDelegate: class{
    func onUserFetched(user: User)
    func onNoUserExistsForID(userID: String)
}



class UserDatabase: NSObject {

    let databaseReference: DatabaseReference
    let userID: String


    init(databaseReference: DatabaseReference, userID: String) {

        self.databaseReference = databaseReference
        self.userID = userID
    }



    func fetchUsersFor(userIDs: Array<String>, delegate: UserDatabaseDelegate?){
        assert(userIDs.count > 0, "::: SCUserStorage ERROR userIDs array cannot be empty")

        var userID: String
        for i in 0..<userIDs.count {
            userID = userIDs[i]
            fetchUserWith(userID: userID, for: delegate)
        }
    }



    func fetchUserWith(userID:String, for delegate: UserDatabaseDelegate?){

        /* compose reference*/
        let usersReference = self.databaseReference.child(cUsersNode).child(userID).child(cUserDataNode)

        usersReference.observeSingleEvent(of: .value, with: { (snapshot) in


            guard snapshot.exists(), snapshot.value != nil else{
                delegate?.onNoUserExistsForID(userID: userID)
                return
            }

            let user = User.init(withSnapshot: snapshot, userID: userID)
            delegate?.onUserFetched(user: user)
            
        })
    }

}
