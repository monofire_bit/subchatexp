//
//  PrivateChatPreviewViewModel.swift
//  SubChat
//
//  Created by ax on 7/12/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import Firebase
import UIKit


protocol PrivateChatPreviewCellViewModelDelegate : class {
    func onImageFetched(viewModel: PrivateChatPreviewCellViewModel)
    func delegateEntity () -> PrivateChatPreviewCellViewModelDelegate
}


protocol PrivateChatPreviewCellViewModelUIEventsDelegate : class {
    func onCellSelected(with privateChatPreview: PrivateChatPreview)
}



enum PrivateChatPreviewImageStatus {
    case NOT_FETCHED
    case IS_FETCHING
    case USER_HAS_NO_IMAGE
    case HAS_IMAGE
}


class PrivateChatPreviewCellViewModel {

    weak var delegate: PrivateChatPreviewCellViewModelDelegate?
    weak var uiEventsDelegate : PrivateChatPreviewCellViewModelUIEventsDelegate?

    /* cell identification */
    var linkedCellIndexPathTag: IndexPath?


    /* image */

    fileprivate var imageStorage : ImageStorage!
    fileprivate var imageData : Data?
    fileprivate var imageFetchGroup: DispatchGroup?
    fileprivate(set) var placeholderImage: UIImage?
    private let cPlaceholderImageSize = CGSize.init(width: 50.0, height: 50.0)

    var counterpartyUserImageData : Data? {
        get{
            guard (self.counterpartyImageStatus == .HAS_IMAGE) else {
                fetchCounterPartyImageIfNeeded()
                return nil
            }
            return self.imageData
        }
    }



    /* cell */
    fileprivate static let cPrivateChatsPreviewCellNibName = "PrivateChatPreviewCell"
    fileprivate static let cPrivateChatsPreviewCellReuseID = "PrivateChatPreviewCellID"


    private(set) var counterpartyUserName: String
    private(set) var distanceToUser: String
    private(set) var counterpartyUserAge: String
    private(set) var lastMessageDate: String
    private(set) var counterpartyImageLink : String?
    fileprivate(set) var counterpartyImageStatus : PrivateChatPreviewImageStatus


    /* model */
    fileprivate let privateChatPreview : PrivateChatPreview








    //MARK: --- Lifecycle

    init (withModel model: PrivateChatPreview, imageStorage: ImageStorage) {

        self.privateChatPreview = model
        self.imageStorage = imageStorage


        /* user name */
        self.counterpartyUserName = model.counterpartyUser?.name ?? "none"


        /* placeholde image */
        self.placeholderImage = PlaceholderImage.init(withSize: cPlaceholderImageSize, text: self.counterpartyUserName)

        /* distance to user */
        self.distanceToUser = {

            return "125m" // TODO: use geoposition service
        }()


        self.counterpartyUserAge = {

            guard let counterpartyUser = model.counterpartyUser,
                let dayOfBirth = counterpartyUser.dayOfBirth,
                let monthOfBirth = counterpartyUser.monthOfBirth,
                let yearOfBirth = counterpartyUser.yearOfBirth
                else{ return "" }


            let now = Date()

            var birthdatDateComponents = DateComponents()
            birthdatDateComponents.day = dayOfBirth
            birthdatDateComponents.month = monthOfBirth
            birthdatDateComponents.year = yearOfBirth

            guard let birthday = Calendar.init(identifier: .gregorian).date(from: birthdatDateComponents) else { return "" }

            let calendar = Calendar.current

            let ageComponents = calendar.dateComponents([.year], from: birthday, to: now)

            guard let age = ageComponents.year else { return "" }

            return String(age) + " yrs"
        }()



        self.lastMessageDate = {

            guard let lastMessageDate = model.privateChatCreationTimestamp else { return "" }

            guard !lastMessageDate.isEmpty else{ return "" }

            let timestampValue = Double (lastMessageDate)!
            let dateTimeStamp : Date = Date.init(timeIntervalSince1970: timestampValue / 1000)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd yyyy HH:mm"
            return dateFormatter.string(from: dateTimeStamp)

        }()


        self.counterpartyImageLink = model.counterpartyUser?.smallAvatarImageLink
        self.counterpartyImageStatus = .NOT_FETCHED
    }







    //MARK: --- Counterparty Image Fetch

    fileprivate func fetchCounterPartyImageIfNeeded(){

        guard self.counterpartyImageStatus == .NOT_FETCHED else {
            self.leaveDispatchGroupIfAny()
            return /* cancels fetch in all other cases  */
        }

        guard let imageLink = self.counterpartyImageLink, !imageLink.isEmpty else {
            self.counterpartyImageStatus = .USER_HAS_NO_IMAGE
            self.leaveDispatchGroupIfAny()
            return
        }

        self.counterpartyImageStatus = .IS_FETCHING
        self.imageStorage.fetchImagesFor(imagesLinks: [imageLink], delegate: self)
    }
}




//MARK: --- Image Storage

extension PrivateChatPreviewCellViewModel: ImageStorageDelegate {

    func onImageFetchError(error: Error?, imageLink: String){
        self.counterpartyImageStatus = .NOT_FETCHED
        self.leaveDispatchGroupIfAny()
    }


    func onImageFetched(imageData: Data, imageLink: String){
        self.imageData = imageData
        self.counterpartyImageStatus = .HAS_IMAGE
        self.leaveDispatchGroupIfAny()



        guard /* check whether image will be updated on right cell */
            let delegateEntity = self.delegate?.delegateEntity() as? UITableViewCell,
            let cellIndexPathTag = delegateEntity.indexPathTag,
            cellIndexPathTag == self.linkedCellIndexPathTag
            else {return}

        self.delegate?.onImageFetched(viewModel: self)
    }
}





//MARK: --- Cell

extension PrivateChatPreviewCellViewModel: CellDeploying {

    static func registerCell(for tableView: UITableView) {

        let cellNib = UINib(nibName:self.cPrivateChatsPreviewCellNibName,bundle:Bundle.main)
        tableView.register(cellNib, forCellReuseIdentifier: cPrivateChatsPreviewCellReuseID)
    }


    func cellInstance(for tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PrivateChatPreviewCellViewModel.cPrivateChatsPreviewCellReuseID, for: indexPath) as! PrivateChatPreviewCell

        cell.indexPathTag = indexPath
        self.linkedCellIndexPathTag = indexPath

        cell.setupWithViewModel(viewModel: self)
        return cell
    }

    func onCellSelected(){
        self.uiEventsDelegate?.onCellSelected(with: self.privateChatPreview)
    }
}




//MARK: --- Image Fetch With Dispatch Group

extension PrivateChatPreviewCellViewModel{
    
    func fetchCounterPartyImageInGroup(dispatchGroup: DispatchGroup){
        self.imageFetchGroup = dispatchGroup
        self.fetchCounterPartyImageIfNeeded()
    }
    
    
    func leaveDispatchGroupIfAny(){
        guard let dispatchGroup = self.imageFetchGroup else{
            return
        }
        dispatchGroup.leave()
        self.imageFetchGroup = nil
    }
}




