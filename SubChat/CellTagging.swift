//
//  CellTagging.swift
//  SubChat
//
//  Created by ax on 8/18/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import UIKit

protocol CellTagging: class {
    var indexPathTag: IndexPath? { get set }
}

