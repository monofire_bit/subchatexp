//
//  NavigationCoordinator+PeopleListUIEvents.swift
//  SubChat
//
//  Created by ax on 8/10/17.
//  Copyright © 2017 SubChat. All rights reserved.
//




extension NavigationCoordinator : PeopleListViewModelUIEventsDelegate{

    func onPrivateChatsPreviewsListButtonClick() {
        self.showPrivateChatsPreviewsList(for: self.testUserID)
    }


    func onMyProfileButtonClick(){
        print ("::: PeopleList -> onMyProfileButtonClick ")
    }

    func onPeopleSementSelected(){
        print ("::: PeopleList -> onPeopleSementSelected ")
    }

    func onChatsSementSelected(){
        print ("::: PeopleList -> onChatsSementSelected ")
    }

}
