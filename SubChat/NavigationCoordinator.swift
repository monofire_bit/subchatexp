//
//  NavigationCoordinator.swift
//  SubChat
//
//  Created by ax on 7/24/17.
//  Copyright © 2017 SubChat. All rights reserved.
//


import UIKit
class NavigationCoordinator{

    fileprivate(set) var navigationController: UINavigationController

    private let application: Application

    //TODO: to be retrieved throught auth process. Pls. update after test DB reset

    fileprivate(set) var testUserID = "-KrkjCLiU50psSjGqYZw"



    //MARK: --- Lifecycle

    init(window: UIWindow, navigationController: UINavigationController, application: Application) {
        self.application = application
        self.navigationController = navigationController
        window.rootViewController = self.navigationController
        window.makeKeyAndVisible()
    }



    //MARK: --- Initial Screen

    func onApplicationLaunch() {

        //self.resetTestDB()
        self.showPeopleList()
    }



    //TODO: Testing. Remove on prod
    func resetTestDB(){
        let demo = FirebaseDemoExamples()
        demo.resetDB()
        demo.populateDevDB()
    }

    

    //MARK: --- Navigation

    func showPeopleList(){

        let peopleListViewModel = PeopleListViewModel()

        /* subscribe */
        peopleListViewModel.uiEventsDelegate = self

        let peopleListViewController = PeopleListViewController.init(peopleListViewModel:peopleListViewModel)
        self.navigationController.pushViewController(peopleListViewController, animated: true)
    }



    func showPrivateChatsPreviewsList(for userID : String){

        let userDatabase = UserDatabase.init(databaseReference: self.application.firebaseService.databaseReference,
                                             userID: userID)

        let privateChatDatabase = PrivateChatDatabase.init(databaseReference: self.application.firebaseService.databaseReference)

        let privateChatsPreviewsList = PrivateChatsPreviewsList.init(withUserID: userID,
                                                                     userDatabase: userDatabase,
                                                                     privateChatDatabase: privateChatDatabase)

        let privateChatsPreviewsListViewModel = PrivateChatsPreviewsListViewModel.init(withModel: privateChatsPreviewsList,
                                                                                       imageStorage: self.application.firebaseService.imageStorage,
                                                                                       maxImagesNumberToBePrefetched: 7)
        /* subscribe */
        privateChatsPreviewsListViewModel.uiEventsDelegate = self

        let privateChatsPreviewsListViewController = PrivateChatsPreviewsListViewController.init(viewModel: privateChatsPreviewsListViewModel)
        self.navigationController.pushViewController(privateChatsPreviewsListViewController, animated: true)
    }


    func showPrivateChat(for userID : String){

        let privateChatViewController = PrivateChatViewController.init()
        privateChatViewController.navigationItem.title = userID
        self.navigationController.pushViewController(privateChatViewController, animated: true)
        
    }
    
}







