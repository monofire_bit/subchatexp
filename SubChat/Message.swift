//
//  Message.swift
//  SubChat
//
//  Created by ax on 6/21/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import Firebase

struct Message {


    let ID : String
    let creationTimestamp : String
    let text : String


    //MARK: --- Lifecycle

    
    init(withSnapshot snapshot: DataSnapshot) {
        assert(snapshot.exists() , "SCMessage ERROR : snapshot does not exist")

        let snapshotDictionary = snapshot.value as! [String: AnyObject]

        let messageID = Array(snapshotDictionary.keys)[0]
        let messageDictionary = Array(snapshotDictionary.values)[0]

        self.ID = messageID

        let timestamp : NSNumber = messageDictionary[kMessageCreationTimestamp] as! NSNumber
        self.creationTimestamp = timestamp.stringValue
        
        self.text = messageDictionary[kMessageText] as! String
    }



    func firebaseValue() -> [String:Any?] {

        let dict : [String : Any?] = [
            kMessageCreationTimestamp: ServerValue.timestamp(),
            kMessageText : self.text
            ]
        return dict
    }




    /* TODO: remove in prod   for testing purposes only */

    init (fakeRandomMessage foo: String) {
        self.ID = ""
        self.creationTimestamp = ""
        self.text = "What is Lorem Ipsum"+String(arc4random())+"? Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the"
    }
    

    
    
}

