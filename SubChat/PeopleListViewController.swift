//
//  PeopleListViewController.swift
//  SubChat
//
//  Created by ax on 8/3/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import UIKit

class PeopleListViewController: UIViewController {


    var peopleListViewModel : PeopleListViewModel!


    //MARK: --- Lifecycle

    convenience init(peopleListViewModel: PeopleListViewModel) {
        self.init(nibName: nil, bundle: nil)
        self.peopleListViewModel = peopleListViewModel
    }



    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationItem()
    }



    //MARK: --- Navigation Item Styling

    func setNavigationItem(){
        /* segmented control */

        let titleViewFrame = CGRect.init(x: 0.0, y: 0.0, width: 120, height: 29)
        let titleView = UIView.init(frame: titleViewFrame)
        let segmentedControlFrame = CGRect.init(x: titleViewFrame.size.width / 2 - 121 / 2, y: 0.0, width: 121, height: 29)

        let color =  UIColor(colorLiteralRed: 92.0 / 255.0, green: 230.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
        let segmentedControl = SegmentedControl.init(withFrame: segmentedControlFrame,
                                                     color: color,
                                                     segmentsText: ["People","Chats"],
                                                     target: self,
                                                     action: #selector(onSegmentControlASelected(segmentedControl:)))

        segmentedControl.selectedSegmentIndex = 0
        titleView.addSubview(segmentedControl)
        self.navigationItem.titleView = titleView



        /* right button */

        let rightButton = UIButton.init(type: .custom)
        rightButton.setImage(UIImage.init(named: "chatIcon"), for: .normal)
        rightButton.frame = CGRect.init(x: 0.0, y: 0.0, width: 20, height: 20)
        rightButton.addTarget(self, action: #selector(onRightButtonClick), for: .touchUpInside)

        let rigthButtonItem = UIBarButtonItem.init(customView: rightButton)
        self.navigationItem.rightBarButtonItem = rigthButtonItem


        /* left button */

        let leftButton = UIButton.init(type: .custom)
        leftButton.setImage(UIImage.init(named: "profileIcon"), for: .normal)
        leftButton.frame = CGRect.init(x: 0.0, y: 0.0, width: 20, height: 20)
        leftButton.addTarget(self, action: #selector(onLeftButtonClick), for: .touchUpInside)

        let leftButtonItem = UIBarButtonItem.init(customView: leftButton)
        self.navigationItem.leftBarButtonItem = leftButtonItem
    }




    //MARK: --- UI Events 

    func onSegmentControlASelected(segmentedControl: UISegmentedControl){
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            self.peopleListViewModel.onPeopleSementSelected()

        default:
            self.peopleListViewModel.onChatsSementSelected()
        }
    }

    func onRightButtonClick() {
        self.peopleListViewModel.onPrivateChatsPreviewsListButtonClick()
        
    }
    
    
    func onLeftButtonClick(){
        self.peopleListViewModel.onMyProfileButtonClick()
    }
    
    
}

