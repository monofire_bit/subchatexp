//
//  PrivateChatPreview.swift
//  SubChat
//
//  Created by ax on 6/21/17.
//  Copyright © 2017 SubChat. All rights reserved.
//


import Foundation

struct PrivateChatPreview {

    var privateChatCreationTimestamp : String?
    var lastMessage : Message?
    var counterpartyUser : User?
}


