//
//  PrivateChatsPreviewsListViewController.swift
//  SubChat
//
//  Created by ax on 7/12/17.
//  Copyright © 2017 SubChat. All rights reserved.
//


import UIKit


class PrivateChatsPreviewsListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    fileprivate var viewModel : PrivateChatsPreviewsListViewModel!
    private let cEstimatedRowHeight : CGFloat = 62


    fileprivate var searchController : UISearchController!

    //TODO: REMOVE IN PROD TESTING
    var taskStartTimestamp : Date?


    //MARK: --- Lifecycle

    convenience init(viewModel: PrivateChatsPreviewsListViewModel) {
        self.init(nibName: nil, bundle: nil)

        /* view model */
        self.viewModel = viewModel
        self.viewModel.delegate = self

        /* UISearchController */
        self.searchController = UISearchController.init(searchResultsController: nil)
        self.searchController.searchResultsUpdater = self
        self.searchController.delegate = self
        self.searchController.dimsBackgroundDuringPresentation = false

    }

    deinit {
        self.searchController.view.removeFromSuperview()
    }



    override func viewDidLoad(){
        super.viewDidLoad()

        /* table view */
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.viewModel.tableCellViewModelTypes.forEach { $0.registerCell(for: self.tableView) }
        self.tableView.estimatedRowHeight = cEstimatedRowHeight
        self.tableView.accessibilityIdentifier = cPrivateChatsPreviewsListTableView /* ui testing id */

        /* UISearchController */
        let searchBarFrame = CGRect.init(x: 0.0, y: 0.0, width: self.tableView.frame.size.width, height: 44)
        self.searchController.searchBar.frame = searchBarFrame
        self.tableView.tableHeaderView =  self.searchController.searchBar
        self.automaticallyAdjustsScrollViewInsets = false
        let cInsetTolerance : CGFloat = 2.0
        self.tableView.contentInset = UIEdgeInsets.init(top: searchBarFrame.size.height - cInsetTolerance, left: 0.0, bottom: 0.0, right: 0.0)


        /* fetch data */
        self.tableView.isHidden = true /* hide table during load */
        self.showTitleNotice("Updating... ", withActivityIndicator: true)

        taskStartTimestamp = Date() //TODO: remove in prod. testing only
        self.viewModel.fetchPrivateChatsPreviews()
    }
}





//MARK: --- Table

extension PrivateChatsPreviewsListViewController : UITableViewDataSource{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.cellViewModels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellViewModel = self.viewModel.cellViewModels[indexPath.row]
        let cell = cellViewModel.cellInstance(for: tableView, indexPath: indexPath)
        return cell
    }
}

extension PrivateChatsPreviewsListViewController : UITableViewDelegate{

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let cellViewModel = self.viewModel.cellViewModels[indexPath.row]
        cellViewModel.onCellSelected()
    }
}


//MARK: --- View Model

extension PrivateChatsPreviewsListViewController : PrivateChatsPreviewsListViewModelDelegate{

    func onNoPrivateChatsExist(){
        self.showTitleNotice("No chats so far", withActivityIndicator: false)
    }

    func onPrivateChatsPreviewsListUpdated(){

        print("::: Fetch execution time ->  \(Date().timeIntervalSince(self.taskStartTimestamp!)) sec")
        self.tableView.isHidden = false /* show table after load */
        self.tableView.reloadData()
        self.hideTitleNotice()
        self.showMainTitle()
    }
}




//MARK: --- Search

extension PrivateChatsPreviewsListViewController : UISearchResultsUpdating {

    func updateSearchResults(for searchController: UISearchController) {
        self.viewModel.filterPrivateChatsPreviewsBy(counterpartyUserName: searchController.searchBar.text)
    }
}

extension PrivateChatsPreviewsListViewController : UISearchControllerDelegate {

    func didDismissSearchController(_: UISearchController){
        self.viewModel.resetFilter()
    }

}



//MARK: --- Title

extension PrivateChatsPreviewsListViewController{

    func showTitleNotice(_ notice: String, withActivityIndicator hasIndicator: Bool){

        self.navigationItem.titleView = nil

        let activityTitleView = ActivityTitleView.init(title: notice, hasActivityIndicator: hasIndicator)

        if (hasIndicator){
            activityTitleView.startActivityIndicator()
        }

        self.navigationItem.titleView = activityTitleView
    }

    func hideTitleNotice(){
        self.navigationItem.titleView = nil
    }
    
    
    func showMainTitle(){
        self.navigationItem.title = "Chats"
        self.navigationItem.backBarButtonItem = UIBarButtonItem.init(title: "Back", style: .plain, target: nil, action: nil)
    }
}






