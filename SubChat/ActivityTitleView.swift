//
//  ActivityTitleView.swift
//  SubChat
//
//  Created by ax on 7/31/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import UIKit


class ActivityTitleView : UIView {

    private var activityIndicatorView : UIActivityIndicatorView?

    //MARK: --- Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    convenience init(title: String, hasActivityIndicator: Bool) {

        /* size constants */
        let cActivityIndicatorSize = CGSize.init(width: 14, height: 14)
        let cSpaceBetweenLabelAndIndicator : CGFloat = 8.0
        var spaceBetweenLabelAndIndicator : CGFloat = 0.0


        let cTitleLabelWidth : CGFloat = 15.0


        /* activity indicator */
        var activityIndicatorView : UIActivityIndicatorView?
        var activityIndicatorFrame = CGRect.zero

        if (hasActivityIndicator){
            activityIndicatorView = UIActivityIndicatorView.init(activityIndicatorStyle: .gray)
            activityIndicatorFrame = CGRect.init(x: 0, y: 0, width: cActivityIndicatorSize.width, height: cActivityIndicatorSize.height)
            activityIndicatorView?.frame = activityIndicatorFrame
            spaceBetweenLabelAndIndicator = cSpaceBetweenLabelAndIndicator
        }


        /* label */
        let titleLabel = UILabel()
        titleLabel.text = title
        titleLabel.font = UIFont.systemFont(ofSize: 14)


        let titleLabelPositionX = activityIndicatorFrame.size.width + spaceBetweenLabelAndIndicator
        let titleLabelPositionY = activityIndicatorFrame.origin.y

        let titleLabelFittingSize = titleLabel.sizeThatFits(CGSize(width: cTitleLabelWidth,
                                                                   height: cActivityIndicatorSize.height))

        titleLabel.frame = CGRect(x: titleLabelPositionX,
                                  y: titleLabelPositionY,
                                  width: titleLabelFittingSize.width,
                                  height: titleLabelFittingSize.height)



        /* frame */
        let titleViewFrame = CGRect.init(x: 0,
                                         y: cActivityIndicatorSize.height / 2,
                                         width: activityIndicatorFrame.size.width + spaceBetweenLabelAndIndicator + titleLabelFittingSize.width,
                                         height: cActivityIndicatorSize.height)


        /* init */
        self.init(frame: titleViewFrame)
        self.activityIndicatorView = activityIndicatorView


        /* compose */
        self.addSubview(titleLabel)

        guard let activity = activityIndicatorView else {
            return
        }
        self.addSubview(activity)
    }
    
    
    func startActivityIndicator(){
        self.activityIndicatorView?.startAnimating()
    }
    
}

