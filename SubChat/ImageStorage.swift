//
//  SCImageStorage.swift
//  SubChat
//
//  Created by ax on 5/23/17.
//  Copyright © 2017 SubChat. All rights reserved.
//


import Firebase


@objc protocol ImageStorageDelegate: class {
    func onImageFetchError(error: Error?, imageLink: String)
    func onImageFetched(imageData: Data, imageLink: String)
    @objc optional func onImageFetchProgress(progress: Float, imageLink: String)
}



class ImageStorage {


    private let maxImageSidePixels: Int = 1024

    let storageTasksQueue = DispatchQueue.init(label: "image.storage.tasks.queue", attributes: .concurrent)

    private let storage: Storage



    //MARK: --- Lifecycle

    init(storage: Storage) {
        self.storage = storage
        storage.callbackQueue = storageTasksQueue /* set callbacks queue */
    }



    //MARK: --- Fetch

    func fetchImagesFor(imagesLinks: Array<String>, delegate : ImageStorageDelegate?){

        assert(imagesLinks.count > 0, "::: SCImageStorage ERROR imagesLinks array cannot be empty")

        var newDownloadTask: StorageDownloadTask

        for imageLink in imagesLinks {

            newDownloadTask = self.downloadTaskWith(imageLink: imageLink, delegate: delegate)

            if  (delegate?.onImageFetchProgress == nil){continue}

            self.subscribeOnProgressEventFor(storageDownloadTask: newDownloadTask,
                                             for: imageLink,
                                             delegate: delegate)
        }

    }



    //MARK: --- Download Task

    /* creates one download task */
    private func downloadTaskWith(imageLink: String, delegate : ImageStorageDelegate?) -> StorageDownloadTask{


        let storageReference = self.storage.reference(forURL: imageLink)
        let maxSize = (Int64)(maxImageSidePixels * maxImageSidePixels)

        let downloadTask = storageReference.getData(maxSize: maxSize) { (data, error) in /* overriden in mock, returns fake download task */


            guard let receivedData = data, receivedData.count > 0, error == nil  else {


                delegate?.onImageFetchError(error: error, imageLink: imageLink)


                return
            }


            delegate?.onImageFetched(imageData: receivedData, imageLink: imageLink)

        }
        return downloadTask
    }




    //MARK: --- Download Progress

    func subscribeOnProgressEventFor(storageDownloadTask: StorageDownloadTask,
                                     for imageLink: String,
                                     delegate : ImageStorageDelegate?){

        /* called on storageTasksQueue */
        storageDownloadTask.observe(.progress, handler: { [unowned self] (snapshot) in

            self.sendProgressTo(delegate: delegate,
                                snapshot: snapshot,
                                for: imageLink)
        })
    }


    func sendProgressTo(delegate : ImageStorageDelegate?,
                        snapshot: StorageTaskSnapshot,
                        for imageLink: String){
        
        let progress: Float =  Float((snapshot.progress?.completedUnitCount)!) / Float((snapshot.progress?.totalUnitCount)!)
        
        
        delegate?.onImageFetchProgress?(progress: progress, imageLink: imageLink)

        
    }
    
}
