    //
    //  PrivateChatPreviewCell.swift
    //  SubChat
    //
    //  Created by ax on 7/12/17.
    //  Copyright © 2017 SubChat. All rights reserved.
    //

    import UIKit

    class PrivateChatPreviewCell: UITableViewCell {

        @IBOutlet weak var counterpartyUserImageView: UIImageView!
        @IBOutlet weak var counterpartyUserNameLabel: UILabel!
        @IBOutlet weak var distanceToUserLabel: UILabel!
        @IBOutlet weak var counterpartyUserAgeLabel: UILabel!
        @IBOutlet weak var lastMessageDateLabel: UILabel!


        //MARK: --- Lifecycle

        override func awakeFromNib() {
            super.awakeFromNib()
            self.counterpartyUserImageView.clipsToBounds = true

            /* performance */
            self.counterpartyUserImageView.isOpaque = true
        }


        func setupWithViewModel(viewModel : PrivateChatPreviewCellViewModel){

            viewModel.delegate = self

            self.setCounterpartyUserImageWithViewModel(viewModel: viewModel)
            self.counterpartyUserNameLabel.text = viewModel.counterpartyUserName
            self.distanceToUserLabel.text = viewModel.distanceToUser
            self.counterpartyUserAgeLabel.text = viewModel.counterpartyUserAge
            self.lastMessageDateLabel.text = viewModel.lastMessageDate
        }



        fileprivate func setCounterpartyUserImageWithViewModel(viewModel : PrivateChatPreviewCellViewModel){

            guard  let userImageData = viewModel.counterpartyUserImageData else {
                self.setPlaceholderImageWithViewModel(viewModel: viewModel)
                return
            }

            guard let userImage = UIImage.init(data: userImageData) else {
                self.setPlaceholderImageWithViewModel(viewModel: viewModel)
                return
            }

            /* dispatch on the main thread */
            DispatchQueue.main.async {
                self.counterpartyUserImageView.image = userImage.roundedImage()
            }
        }


        func setPlaceholderImageWithViewModel(viewModel : PrivateChatPreviewCellViewModel){
            guard  let image = viewModel.placeholderImage else {return}

            /* called on the main thread */
            self.counterpartyUserImageView.image = image
        }


    }

    
    extension PrivateChatPreviewCell : PrivateChatPreviewCellViewModelDelegate{
        
        func onImageFetched(viewModel: PrivateChatPreviewCellViewModel) {
            self.setCounterpartyUserImageWithViewModel(viewModel: viewModel)
        }

        func delegateEntity () -> PrivateChatPreviewCellViewModelDelegate{
            return self
        }
    }
