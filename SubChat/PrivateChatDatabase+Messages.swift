//
//  PrivateChatDatabase+Messages.swift
//  SubChat
//
//  Created by ax on 6/15/17.
//  Copyright © 2017 SubChat. All rights reserved.
//



protocol PrivateChatDatabaseMessagesDelegate {
    func onLastMessageFetchedForPrivateChat(privateChatID : String, lastMessage: Message)
    func onNoMessagesExistForPrivateChat(privateChatID : String)
}


extension PrivateChatDatabase{

    func fetchLastMessageFor(privateChatID: String, delegate: PrivateChatDatabaseMessagesDelegate?){


        /* compose reference */
        let privateChatMessagesReferenceQuery = databaseReference.child(cPrivateChatsNode).child(privateChatID).child(cPrivateChatMessagesNode).queryLimited(toLast: 1)

        /* request data */
        privateChatMessagesReferenceQuery.observeSingleEvent(of: .value, with: { (snapshot) in


            guard snapshot.exists(), snapshot.value != nil else{
                delegate?.onNoMessagesExistForPrivateChat(privateChatID: privateChatID)
                return
            }

            let lastMessage = Message.init(withSnapshot: snapshot)
            delegate?.onLastMessageFetchedForPrivateChat(privateChatID: privateChatID, lastMessage: lastMessage)
            
        })
    }
    
    
    
}
