//
//  UIColor+RandomColor.swift
//  SubChat
//
//  Created by ax on 8/11/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import UIKit

extension UIColor{

    static func randomColor() -> UIColor {

        let randomHue = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
        let randomSaturation = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
        let randomBrightness = CGFloat(Float(arc4random()) / Float(UINT32_MAX))

        return UIColor.init(hue: randomHue, saturation: randomSaturation, brightness: randomBrightness, alpha: 1.0)
    }
}


extension UIColor {
    var hsba:(h: CGFloat, s: CGFloat,b: CGFloat,a: CGFloat) {
        var h: CGFloat = 0, s: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
        self.getHue(&h, saturation: &s, brightness: &b, alpha: &a)
        return (h: h, s: s, b: b, a: a)
    }
}
