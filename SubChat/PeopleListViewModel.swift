//
//  PeopleListViewModel.swift
//  SubChat
//
//  Created by ax on 8/10/17.
//  Copyright © 2017 SubChat. All rights reserved.
//


protocol PeopleListViewModelUIEventsDelegate {
    func onPrivateChatsPreviewsListButtonClick()
    func onMyProfileButtonClick()
    func onPeopleSementSelected()
    func onChatsSementSelected()

}


class PeopleListViewModel{

    /* Delegates */
    var uiEventsDelegate : PeopleListViewModelUIEventsDelegate?




    //MARK: --- UI Events
    func onPrivateChatsPreviewsListButtonClick(){
        self.uiEventsDelegate?.onPrivateChatsPreviewsListButtonClick()
    }

    func onMyProfileButtonClick(){
        self.uiEventsDelegate?.onMyProfileButtonClick()
    }

    func onPeopleSementSelected(){
        self.uiEventsDelegate?.onPeopleSementSelected()
    }

    func onChatsSementSelected(){
        self.uiEventsDelegate?.onChatsSementSelected()
    }
}
