//
//  User.swift
//  SubChat
//
//  Created by ax on 6/21/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import Firebase

struct User {


    let ID : String
    let creationTimestamp : String
    let name : String?
    let dayOfBirth : Int?
    let monthOfBirth : Int?
    let yearOfBirth : Int?
    let education : String?
    let job : String?
    let about : String?
    let fbLink : String?
    let avatarImageLink : String?
    let smallAvatarImageLink : String?


    
    //MARK: --- Lifecycle

    init (withSnapshot snapshot: DataSnapshot, userID: String) {

        assert(snapshot.exists() , "SCUser ERROR : snapshot does not exist")
        assert(!userID.isEmpty, "SCUser ERROR : userID is empty")

        let snapshotDictionary = snapshot.value as! [String: AnyObject]

        self.ID = userID

        let timestamp : NSNumber = snapshotDictionary[kUserCreationTimestamp] as! NSNumber
        self.creationTimestamp = timestamp.stringValue

        self.name = snapshotDictionary[kUserName] as? String
        self.dayOfBirth = snapshotDictionary[kDayOfBirth] as? Int
        self.monthOfBirth = snapshotDictionary[kMonthOfBirth] as? Int
        self.yearOfBirth = snapshotDictionary[kYearOfBirth] as? Int
        self.education = snapshotDictionary[kUserEducation] as? String
        self.job = snapshotDictionary[kUserJob] as? String
        self.about = snapshotDictionary[kUserAbout] as? String
        self.fbLink = snapshotDictionary[kUserFbLink] as? String
        self.avatarImageLink =  snapshotDictionary[kUserAvatarImageLink] as? String
        self.smallAvatarImageLink =  snapshotDictionary[kUserSmallAvatarImageLink] as? String
    }


    func firebaseValue() -> [String:Any?] {

        let dict : [String : Any?] = [
            kUserCreationTimestamp: ServerValue.timestamp(),
            kUserName: self.name,
            kDayOfBirth: self.dayOfBirth,
            kMonthOfBirth: self.monthOfBirth,
            kYearOfBirth: self.yearOfBirth,
            kUserEducation: self.education,
            kUserJob: self.job,
            kUserAbout: self.about,
            kUserFbLink: self.fbLink,
            kUserAvatarImageLink: self.avatarImageLink,
            kUserSmallAvatarImageLink: self.smallAvatarImageLink
        ]
        return dict
    }



    /* TODO: remove in prod   for testing purposes only */

    init (fakeRandomUserWithImageLink imageLink: String, smallImageLink: String) {

        self.ID = ""
        self.creationTimestamp = ""
        self.name = "testUserJoe " + String ( arc4random() % 200 )
        self.dayOfBirth = Int(arc4random_uniform(UInt32(1 + 31 - 1))) + 1
        self.monthOfBirth =  Int(arc4random_uniform(UInt32(1 + 12 - 1))) + 1
        self.yearOfBirth =  Int(arc4random_uniform(UInt32(1 + 2000 - 1980))) + 1980
        self.education =  "Joe's education" + String ( arc4random() % 200 )
        self.job = "Joe's job" + String ( arc4random() % 200 )
        self.about = "About Joe" + String ( arc4random() % 200 )
        self.fbLink = "fb.com/joe" + String ( arc4random() % 200 )
        self.avatarImageLink =  imageLink
        self.smallAvatarImageLink = smallImageLink
    }
    
}
