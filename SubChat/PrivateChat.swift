//
//  PrivateChat.swift
//  SubChat
//
//  Created by ax on 6/23/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import Firebase

struct PrivateChat {

    let ID : String
    let creationTimestamp : String

    
    //MARK: --- Lifecycle

    init (withSnapshot snapshot: DataSnapshot, privateChatID: String) {
        assert(snapshot.exists() , "PrivateChat ERROR : snapshot does not exist")
        assert(!privateChatID.isEmpty , "PrivateChat ERROR : privateChatID is empty")

        let snapshotDictionary = snapshot.value as! [String: Any] // was anyObject
        self.ID = privateChatID

        let timestamp : NSNumber = snapshotDictionary[kPrivateChatCreationTimestamp] as! NSNumber
        self.creationTimestamp = timestamp.stringValue
    }



    func firebaseValue() -> [String:Any?] {

        let dict : [String : Any?] = [
            kPrivateChatCreationTimestamp: ServerValue.timestamp(),
            ]
        return dict
    }



    /* TODO: remove in prod   for testing purposes only */

    init (fakeRandomPrivateChat foo: String) {
        self.ID = ""
        self.creationTimestamp = ""
    }
    
    
    
    
}
