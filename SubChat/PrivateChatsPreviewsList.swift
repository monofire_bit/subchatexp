//
//  PrivateChatsPreviewsList.swift
//  SubChat
//
//  Created by ax on 6/21/17.
//  Copyright © 2017 SubChat. All rights reserved.
//



import Foundation

protocol PrivateChatsPreviewsListDelegate: class {

    func onPrivateChatsPreviewsListFetched(privateChatsPreviewsList : Array <PrivateChatPreview>, forUserID userID: String)
    func onNoPrivateChatsExist(forUserID userID: String)
}

class PrivateChatsPreviewsList
{

    weak var delegate : PrivateChatsPreviewsListDelegate?

    /* product */
    var privateChatsPreviewsList : [String : PrivateChatPreview]

    /* models */
    let myUserID : String
    fileprivate let userDatabase : UserDatabase
    fileprivate let privateChatDatabase : PrivateChatDatabase

    /* building */
    var userID_privateChatID_relations : [String:String]
    var buildingStatus : PrivateChatListBuildingStatus

    let concurrentAccessQueueUserPrivateChat = DispatchQueue(
        label: "user_privateChat_relations.access.queue",
        qos: .utility,
        attributes: .concurrent)

    let concurrentAccessQueuePrivateChatsPreviewsList  =  DispatchQueue(
        label: "private_chats_list.access.queue",
        qos: .utility,
        attributes: .concurrent)

    let cFetchTimeoutSeconds = 15.0
    var timerIsArmed = false




    //MARK: --- Lifecycle

    init(withUserID myUserID: String,
         userDatabase: UserDatabase,
         privateChatDatabase: PrivateChatDatabase) {

        self.privateChatsPreviewsList = [String : PrivateChatPreview]()

        self.myUserID = myUserID
        self.userDatabase = userDatabase
        self.privateChatDatabase = privateChatDatabase


        self.userID_privateChatID_relations =  [String:String]()
        self.buildingStatus = PrivateChatListBuildingStatus()
    }



    //MARK: --- Fetch

    func fetchPrivateChatsPreviewsList(){
        self.armTimer()
        self.resetEntityRelations()
        self.resetBuildingStatus()
        self.userDatabase.fetchPrivateChatIDsFor(userID: self.myUserID, delegate: self)
    }
}






//MARK: --- Private Chats IDs Is Fetched
extension PrivateChatsPreviewsList : UserDatabasePrivateChatsDelegate{

    func onPrivateChatIDsFetchedForUser(userID:String, privateChatIDs : Array<String>){
        self.modifyPrivateChatFetchActiveRequestsCount(delta: privateChatIDs.count)
        self.privateChatDatabase.fetchPrivateChatsFor(privateChatIDs: privateChatIDs, delegate: self)
    }

    func onNoPrivateChatsExistForUser(userID:String){
        self.delegate?.onNoPrivateChatsExist(forUserID: userID)
    }
}



//MARK: --- Private Chats Data Is Fetched
extension PrivateChatsPreviewsList : PrivateChatDatabaseDelegate{

    func onPrivateChatFetched(privateChat: PrivateChat){

        self.populatePrivateChatsPreviewsListWithNewPrivateChat(privateChat: privateChat, onCompletionHandler: {


            self.modifyPrivateChatFetchActiveRequestsCount(delta: -1)

            self.modifyMessageFetchActiveRequestsCount(delta: 1)
            self.privateChatDatabase.fetchLastMessageFor(privateChatID: privateChat.ID, delegate: self)

            self.modifyUserIdFetchActiveRequestsCount(delta: 1)
            self.privateChatDatabase.fetchUsersFor(privateChatID: privateChat.ID, delegate: self)

        })
    }

    func onNoPrivateChatExistsForID (privateChatID : String){
        self.modifyPrivateChatFetchActiveRequestsCount(delta: -1)
    }
}



//MARK: --- Last Message For Private Chat Is Fetched
extension PrivateChatsPreviewsList :  PrivateChatDatabaseMessagesDelegate {

    func onLastMessageFetchedForPrivateChat(privateChatID : String, lastMessage: Message){

        self.addMessageDataToPrivateChatsPreviewsList(messageData: lastMessage, privateChatID: privateChatID, onCompletionHandler:{

            self.modifyMessageFetchActiveRequestsCount(delta: -1)

            if (self.buildProcessIsCompleted()){
                self.notifyDelegateWithFetchCompletion()
            }

        })
    }

    func onNoMessagesExistForPrivateChat(privateChatID : String){
        self.modifyMessageFetchActiveRequestsCount(delta: -1)

        if (self.buildProcessIsCompleted()){
            self.notifyDelegateWithFetchCompletion()
        }
    }
}



//MARK: ---  Users IDs For Private Chat Are Fetched
extension PrivateChatsPreviewsList : PrivateChatDatabaseUsersDelegate {


    func onUserIDsFetchedForPrivateChat(privateChatID: String, userIDs: Array <String>){

        let counterpartyUserID = self.myCounterpartyUserIDFromPrivateChatUserIDs(userIDs: userIDs)

        guard !counterpartyUserID.isEmpty else {
            self.modifyUserIdFetchActiveRequestsCount(delta: -1)
            return
        }

        self.setPrivateChatIdRelation(privateChatID:privateChatID, forUserID: counterpartyUserID)
        self.modifyUserIdFetchActiveRequestsCount(delta: -1)

        self.modifyUserFetchActiveRequestsCount(delta: 1)
        self.userDatabase.fetchUserWith(userID: counterpartyUserID, for: self)

    }


    func onNoUsersExistForPrivateChat(privateChatID: String){
        self.modifyUserIdFetchActiveRequestsCount(delta: -1)
    }
}



//MARK: --- User Data For User IDs Is Fetched
extension PrivateChatsPreviewsList : UserDatabaseDelegate {

    func onUserFetched(user: User){

        self.addUserToPrivateChatsPreviewsList(counterpartyUser: user, onCompletionHandler: {

            self.modifyUserFetchActiveRequestsCount(delta: -1)

            if (self.buildProcessIsCompleted()){
                self.notifyDelegateWithFetchCompletion()
            }
            
        })
    }
    
    func onNoUserExistsForID(userID: String){
        self.modifyUserFetchActiveRequestsCount(delta: -1)
        
        if (self.buildProcessIsCompleted()){
            self.notifyDelegateWithFetchCompletion()
        }
    }
}





