//
//  PrivateChatsPreviewsList+Sort.swift
//  SubChat
//
//  Created by ax on 7/11/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

extension PrivateChatsPreviewsList{


    /* sort previews by last message timestamp */
    func sortedPrivateChatPreviewsList( unsortedPrivateChatsPreviewsList : [String : PrivateChatPreview] ) -> Array <PrivateChatPreview>   {

        var privateChatsPreviews = Array <PrivateChatPreview>()
        privateChatsPreviews = Array(unsortedPrivateChatsPreviewsList.values)

        let sortedPreviewsList = privateChatsPreviews.sorted (by: { (first : PrivateChatPreview, second : PrivateChatPreview) -> Bool in

            /* if no messages available sort is made by chat creation timestamp */
            let firstElementTimestamp = (first.lastMessage != nil ? first.lastMessage!.creationTimestamp : first.privateChatCreationTimestamp ?? "0")
            let secondElementTimestamp = (second.lastMessage != nil ? second.lastMessage!.creationTimestamp : second.privateChatCreationTimestamp ?? "0")

            if ( UInt64(firstElementTimestamp)! > UInt64(secondElementTimestamp)! ) {
                return true
            }
            
            return false
        })
        return sortedPreviewsList
    }

}

