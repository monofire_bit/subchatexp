//
//  PrivateChatsPreviewsList+EntityRelations.swift
//  SubChat
//
//  Created by ax on 6/28/17.
//  Copyright © 2017 SubChat. All rights reserved.
//


extension PrivateChatsPreviewsList {


    func setPrivateChatIdRelation (privateChatID : String, forUserID userID : String){

        self.concurrentAccessQueueUserPrivateChat.async (flags: .barrier){
            self.userID_privateChatID_relations[userID] = privateChatID
        }
    }

    func getPrivateChatIDFrom (userID: String) -> String {

        var privateChatID : String!
        self.concurrentAccessQueueUserPrivateChat.sync (flags: .barrier){
            privateChatID = self.userID_privateChatID_relations[userID]!
        }

        return privateChatID
    }


    func resetEntityRelations(){
        self.userID_privateChatID_relations.removeAll()
    }
}
