//
//  NavigationViewController.swift
//  SubChat
//
//  Created by ax on 7/31/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import UIKit

class NavigationViewController: UINavigationController {


    //MARK: --- Lifecycle
    convenience init() {
        self.init(navigationBarClass: nil, toolbarClass: nil)
        self.navigationBar.tintColor = UIColor.darkGray
    }



    //MARK: - Navigation
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {

        self.styleViewController(viewController, root: animated)
        super.pushViewController(viewController, animated: animated)
    }



    //MARK: - View Controller styling
    fileprivate func styleViewController(_ viewController: UIViewController?, root: Bool) {

        let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        backButton.width = 1.0

        viewController?.navigationItem.backBarButtonItem = backButton
    }

}
