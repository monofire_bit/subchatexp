//
//  CellRepresentable.swift
//  SubChat
//
//  Created by ax on 7/25/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import UIKit
import ObjectiveC

protocol CellDeploying {

    /* instantiate */
    static func registerCell(for tableView: UITableView)
    func cellInstance(for tableView: UITableView, indexPath: IndexPath) -> UITableViewCell

    /* ui events */
    func onCellSelected()

}


