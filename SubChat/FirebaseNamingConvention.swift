//
//  FirebaseNamingConvention.swift
//  SubChat
//
//  Created by ax on 6/15/17.
//  Copyright © 2017 SubChat. All rights reserved.
//


/* Nodes */
let cUsersNode = "users_node"
let cUserDataNode = "user_data_node"
let cUserPrivateChatIDsNode = "user_private_chat_IDs_node"


let cPrivateChatsNode = "private_chats_node"
let cPrivateChatDataNode = "private_chat_data_node"
let cPrivateChatMessagesNode = "private_chat_messages_node"
let cPrivateChatUsersNode = "private_chat_users_IDs_node"


/* Message keys */
let kMessageCreationTimestamp = "kMessageCreationTimestamp"
let kMessageText = "kMessageText"

/* User keys */
let kUserCreationTimestamp = "kUserCreationTimestamp"
let kUserName = "kUserName"
let kDayOfBirth = "kDayOfBirth"
let kMonthOfBirth = "kMonthOfBirth"
let kYearOfBirth = "kYearOfBirth"
let kUserEducation = "kUserEducation"
let kUserJob = "kUserJob"
let kUserAbout = "kUserAbout"
let kUserFbLink = "kUserFbLink"
let kUserAvatarImageLink = "kUserAvatarImageLink"
let kUserSmallAvatarImageLink = "kUserSmallAvatarImageLink"



/* PrivateChat keys */
let kPrivateChatCreationTimestamp = "kPrivateChatCreationTimestamp"

