//
//  PrivateChatDatabase.swift
//  SubChat
//
//  Created by ax on 6/15/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import Firebase

protocol PrivateChatDatabaseDelegate {
    func onPrivateChatFetched(privateChat: PrivateChat)
    func onNoPrivateChatExistsForID (privateChatID : String)
}




class PrivateChatDatabase {


    let databaseReference: DatabaseReference


    //MARK: --- Lifecycle

    init(databaseReference: DatabaseReference) {

        self.databaseReference = databaseReference
    }


    //MARK: --- Fetch

    func fetchPrivateChatsFor(privateChatIDs : Array <String>, delegate : PrivateChatDatabaseDelegate?){
        assert(privateChatIDs.count > 0, "::: PrivateChatDatabase ERROR privateChatIDs array cannot be empty")

        for privateChatID in privateChatIDs {
            fetchPrivateChatFor(privateChatID: privateChatID, delegate: delegate)
        }
    }



    func fetchPrivateChatFor(privateChatID: String, delegate: PrivateChatDatabaseDelegate?){

        /* compose reference */
        let privateChatReference = self.databaseReference.child(cPrivateChatsNode).child(privateChatID).child(cPrivateChatDataNode)
        
        /* request data */
        privateChatReference.observeSingleEvent(of: .value, with: { (snapshot) in

            guard snapshot.exists(), snapshot.value != nil else{
                delegate?.onNoPrivateChatExistsForID(privateChatID: privateChatID)
                return
            }

            let privateChat = PrivateChat.init(withSnapshot: snapshot, privateChatID: privateChatID)
            delegate?.onPrivateChatFetched(privateChat: privateChat)


        })
    }



}
