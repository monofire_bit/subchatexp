//
//  PrivateChatsPreviewsList+ChatListOperations.swift
//  SubChat
//
//  Created by ax on 6/28/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import Foundation
extension PrivateChatsPreviewsList{


    func populatePrivateChatsPreviewsListWithNewPrivateChat(privateChat: PrivateChat,
                                                            onCompletionHandler : @escaping () -> Void){

        var privateChatPreview = PrivateChatPreview()
        privateChatPreview.privateChatCreationTimestamp = privateChat.creationTimestamp

        self.concurrentAccessQueuePrivateChatsPreviewsList.async(flags: .barrier) {
            [unowned self] in
            self.privateChatsPreviewsList[privateChat.ID] = privateChatPreview
            onCompletionHandler()
        }
    }


    func addMessageDataToPrivateChatsPreviewsList(messageData : Message,
                                                  privateChatID : String,
                                                  onCompletionHandler : @escaping () -> Void){

        self.concurrentAccessQueuePrivateChatsPreviewsList.async(flags: .barrier) {
            [unowned self] in
            self.privateChatsPreviewsList[privateChatID]?.lastMessage = messageData
            onCompletionHandler()
        }
    }


    func addUserToPrivateChatsPreviewsList(counterpartyUser : User,
                                           onCompletionHandler : @escaping () -> Void){

        let privateChatID = self.getPrivateChatIDFrom(userID: counterpartyUser.ID)
        self.concurrentAccessQueuePrivateChatsPreviewsList.async(flags: .barrier) {
            [unowned self] in
            self.privateChatsPreviewsList[privateChatID]?.counterpartyUser = counterpartyUser
            onCompletionHandler()
        }
    }
}
