//
//  SegmentedControl.swift
//  SubChat
//
//  Created by ax on 8/4/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import UIKit

class SegmentedControl: UISegmentedControl{


    convenience init(withFrame frame: CGRect, color: UIColor, segmentsText: [String], target: Any?, action : Selector) {

        /* designated */
        self.init(items: segmentsText)


        /* setup */
        let cornerRadius = self.bounds.height / 2
        self.layer.cornerRadius = cornerRadius
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = 1
        self.layer.masksToBounds = true

        self.tintColor = color
        self.frame = frame
        self.addTarget(target, action: action, for: .valueChanged)
    }

}
