//
//  RoundedRectView.swift
//  SubChat
//
//  Created by ax on 7/27/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import UIKit

class RoundedRectView: UIView {

    //MARK: --- Lifecycle

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        drawRoundedRect(parentSize: self.bounds.size,
                        pathColor: UIColor(colorLiteralRed: 92.0 / 255.0, green: 230.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0),
                        lineWidth: 1.0,
                        cornerSize: CGSize(width: 8, height: 8))
    }


    //MARK: --- Drawing

    private func drawRoundedRect(parentSize: CGSize, pathColor: UIColor, lineWidth: CGFloat, cornerSize : CGSize){

        /* path */
        let pathWidth = parentSize.width - lineWidth
        let pathHeight = parentSize.height - lineWidth
        let pathFrame = CGRect(x: 0, y: 0, width: pathWidth, height: pathHeight)
        let rectanglePath = CGPath(roundedRect: pathFrame,
                                   cornerWidth: cornerSize.width,
                                   cornerHeight: cornerSize.height,
                                   transform: nil)

        /* shape layer */
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = rectanglePath
        shapeLayer.strokeColor = pathColor.cgColor
        shapeLayer.fillColor = UIColor.white.cgColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        shapeLayer.position = CGPoint(x: lineWidth / 2, y: lineWidth / 2)

        self.layer.addSublayer(shapeLayer)
    }
}
