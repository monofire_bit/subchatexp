//
//  PlaceholderImage.swift
//  SubChat
//
//  Created by ax on 8/14/17.
//  Copyright © 2017 SubChat. All rights reserved.
//

import UIKit

class PlaceholderImage: UIImage {

    //MARK: --- Lifecycle

    convenience init? (withSize size: CGSize, text: String){

        assert(text.characters.count > 0,"ERRO! Placeholder image text length is not valid ")

        /* calculate random color */
        let randomHue = CGFloat(Float(arc4random()) / Float(UINT32_MAX) / 2.0 )
        let randomSaturation = CGFloat(Float(arc4random()) / Float(UINT32_MAX) )


        /* calculate gradient colors */
        let bottomColor = UIColor.init(hue: randomHue, saturation: randomSaturation, brightness: 0.8, alpha: 1.0)
        let topColor = UIColor.init(hue: bottomColor.hsba.h,
                                    saturation: bottomColor.hsba.s / 2.0,
                                    brightness: bottomColor.hsba.b ,
                                    alpha: 1.0)


        /* background layer */
        let backgroudLayer = CAShapeLayer.init()
        backgroudLayer.frame = CGRect.init(x: 0.0, y: 0.0, width: size.width, height: size.height)
        backgroudLayer.backgroundColor = UIColor.white.cgColor



        /* gradient layer */
        let gradientLayer = CAGradientLayer.init()
        gradientLayer.frame = CGRect.init(x: 0.0, y: 0.0, width: size.width, height: size.height)
        gradientLayer.masksToBounds = true
        gradientLayer.cornerRadius = size.width / 2.0
        gradientLayer.colors = [topColor.cgColor,bottomColor.cgColor]



        /* create text */
        var shortName : String = String(describing: text.characters.first!)

        if let blankSpaceIndex = text.characters.index(of: " "){
            shortName += String( text[text.index(blankSpaceIndex, offsetBy: 1)])
        }

        else if let dotIndex  = text.characters.index(of: "."){
            shortName += String( text[text.index(dotIndex, offsetBy: 1)] )
        }


        /* text layer */
        let textLayer = CATextLayer()
        textLayer.frame = CGRect.init(x: 0.0, y: size.height / 5.0, width: size.width, height: size.height)
        textLayer.string = shortName.uppercased()
        textLayer.foregroundColor = UIColor.white.cgColor
        textLayer.alignmentMode = kCAAlignmentCenter
        textLayer.fontSize = size.height / 2.0
        textLayer.masksToBounds = true
        textLayer.cornerRadius = size.width / 2.0


        /* compose */
        gradientLayer.addSublayer(textLayer)
        backgroudLayer.addSublayer(gradientLayer)

        
        /* create image */
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        backgroudLayer.render(in: UIGraphicsGetCurrentContext()!)
        let renderedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()


        guard let placeholderCGImage = renderedImage?.cgImage else {return nil}

        self.init(cgImage: placeholderCGImage)
    }



}
