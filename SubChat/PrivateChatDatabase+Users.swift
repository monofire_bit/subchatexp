//
//  PrivateChatDatabase+Users.swift
//  SubChat
//
//  Created by ax on 6/15/17.
//  Copyright © 2017 SubChat. All rights reserved.
//


protocol PrivateChatDatabaseUsersDelegate {

    func onUserIDsFetchedForPrivateChat(privateChatID: String, userIDs: Array <String>)
    func onNoUsersExistForPrivateChat(privateChatID: String)


}


extension PrivateChatDatabase{

    func fetchUsersFor(privateChatID: String, delegate: PrivateChatDatabaseUsersDelegate?){

        /* compose reference*/
        let privateChatUsersReference = databaseReference.child(cPrivateChatsNode).child(privateChatID).child(cPrivateChatUsersNode)

        /* request data */
        privateChatUsersReference.observeSingleEvent(of: .value, with: { (snapshot) in

            guard snapshot.exists(), snapshot.value != nil else{
                delegate?.onNoUsersExistForPrivateChat(privateChatID: privateChatID)
                return
            }
            
            let userIDs = snapshot.value as! Array<String>
            assert(userIDs.count == 2, "PrivateChatDatabase ERROR! private chat should have only 2 users, userIDs count should be 2")
            delegate?.onUserIDsFetchedForPrivateChat(privateChatID: privateChatID, userIDs: userIDs)

        })
    }
    
}
